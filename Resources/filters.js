function filters(){
	var w = Ti.UI.createWindow({
        theme:"Theme.AppCompat.Translucent.NoTitleBar",
        backgroundImage:"/images/bg.jpg",
        orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
    });

    var v = Ti.UI.createView({
        backgroundImage:"/images/bg.jpg",
        width:"100%",height:"100%",
        opacity:0.001,top:"50%"
    });

    var topView = ui.view({
        backgroundColor:'#2b2831',
        width:'100%',height:'10%',
        top:0,left:0
    });
    v.add(topView);

    w.addEventListener("open",function(e){
        v.animate({duration:200,opacity:1,top:'0%'});
    });
    
    var filtersLogo = Titanium.UI.createView({
        backgroundImage:'/images/filterTitle.png',
        width:58,height:14,left:'2%',
        backgroundColor:'transparent'
    });
    topView.add(filtersLogo);
    filtersLogo.addEventListener('click',function(e){w.close();});

    var tableContainer = ui.view({
        width:"100%",height:"90%",
        top:"10%",backgroundColor:'#34303e',
    });
    v.add(tableContainer);
    
    var tableFilters = Titanium.UI.createTableView({
        width:"100%",height:"100%",
        top:0,data:[],separatorColor:'transparent',
        backgroundColor:'transparent',
    });    

    var filters = [
        {i:"ic_fil_all_events_normal.png",s:"ic_fil_all_events_active.png",w:149,f:"All Events"},
        {i:"ic_fil_car_normal.png",s:"ic_fil_car_active.png",w:158,f:"Car Events"},
        {i:"ic_fil_edm_normal.png",s:"ic_fil_edm_active.png",w:78,f:"EDM"},
        {i:"ic_fil_farmers_normal.png",s:"ic_fil_farmers_active.png",w:228,f:"Farmers Market"},
        {i:"ic_fil_food_normal.png",s:"ic_fil_food_active.png",w:182,f:"Food Specials"},
        {i:"ic_fil_happy_normal.png",s:"ic_fil_happy_active.png",w:173,f:"Happy Hours"},
        {i:"ic_fil_karaoke_normal.png",s:"ic_fil_karaoke_active.png",w:124,f:"Karaoke"},
        {i:"ic_fil_live_normal.png",s:"ic_fil_live_active.png",w:144,f:"Live Music"},
        {i:"ic_fil_other_normal.png",s:"ic_fil_other_active.png",w:102,f:"Others"},
        {i:"ic_fil_specials_normal.png",s:"ic_fil_specials_active.png",w:128,f:"Specials"},
        {i:"ic_fil_sporting_normal.png",s:"ic_fil_sporting_active.png",w:221,f:"Sporting Events"},
    ];

    var data = [];

    for(var i = 0 ; i<filters.length ; i++){

        var filter = Titanium.UI.createImageView({
            image:"/images/"+filters[i].i,
            width:filters[i].w,
            selectedImage:"/images/" + filters[i].s,
            originalImage:"/images/" + filters[i].i,
        });

        var row = Titanium.UI.createTableViewRow({
            backgroundColor:'transparent',
            selectedBackgroundColor:'transparent',
            id:i,category:filters[i].f,filter:filter,
            height:43,selected:false,
        });

        if(api.currentFilter.indexOf(filters[i].f) != -1){
            row.selected = true;
            filter.image = filter.selectedImage;
        }
        row.add(filter);
        data.push(row);
    }
    tableFilters.setData(data);

    tableFilters.addEventListener("click",function(e){
        if(e.row.category == "All Events"){
            clearSelectedRows();
            return;
        }else{
            if(!e.row.selected){
                tableFilters.data[0].rows[0].filter.image = tableFilters.data[0].rows[0].filter.originalImage;
                e.row.filter.image = e.row.filter.selectedImage;
                e.row.selected = true;
                if(api.currentFilter == "All Events"){
                    api.currentFilter = [e.row.category];
                }else{
                    api.currentFilter.push(e.row.category);
                }
            }else{
                var index = api.currentFilter.indexOf(e.row.category);
                api.currentFilter.splice(index,1);
                e.row.filter.image = e.row.filter.originalImage;
                e.row.selected = false;
                if(api.currentFilter.length == 0) {
                    clearSelectedRows();
                }
            }
        }
    });

    function clearSelectedRows(){
        for(var c = 0 ;c < tableFilters.data[0].rows.length; c++){
            var r = tableFilters.data[0].rows[c];
            r.filter.image = r.filter.originalImage;
            r.selected = false;
        }
        tableFilters.data[0].rows[0].filter.image = tableFilters.data[0].rows[0].filter.selectedImage;
        api.currentFilter = "All Events";
    }

    tableContainer.add(tableFilters);
    w.add(v);

	return w;
}

module.exports = filters;