function chooseevent(evts){
    var w;
    var btClose;
    var data = [];

    var btClose = Titanium.UI.createButton({
        width:13,height:22,
        backgroundImage:"/backw.png"
    });
    btClose.addEventListener("click",function(e){
        w.close();
    });

    var w = Ti.UI.createWindow({
        theme:"Theme.AppCompat.Translucent.NoTitleBar",
        backgroundImage:"/images/bg.jpg",
        orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
    });

    w.addEventListener('open',function(e){
        progressIndicator.hide();
    });
    var topView = ui.view({
        backgroundColor:'#2b2831',
        width:'100%',height:'10%',
        top:0,left:0
    });
    w.add(topView);

    var chooseEventTitle = Titanium.UI.createView({
        backgroundImage:'/images/listTitle.png',
        width:106,height:14,left:'2%',
        backgroundColor:'transparent'
    });
    topView.add(chooseEventTitle);
    chooseEventTitle.addEventListener('click',function(e){w.close();});


    //LIST WITH EVENTS
    var listEvents = Titanium.UI.createTableView({
        width:Ti.UI.FILL,
        height:Ti.UI.FILL,
        backgroundColor:"transparent",
        data:[],left:0,top:50
    });

    listEvents.addEventListener("click",function(e){
        var profileW = require("eventprofile")(e.rowData.obj);
        profileW.open();
    });

    data = [];
    events = evts;

    for(var i = 0;i<events.length;i++){

        var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

        var eventDateObj = events[i].get("date");

        var day = (eventDateObj.getDate()<10) ? "0"+eventDateObj.getDate() : eventDateObj.getDate();
        var weekDay = days[ eventDateObj.getDay() ];
        var month = months[eventDateObj.getMonth()];
        var year = eventDateObj.getFullYear();
        var hours = ""+ui.formatAMPM(eventDateObj).hour;
        var minutes = ""+eventDateObj.getMinutes();
        var ampm = ""+ui.formatAMPM(eventDateObj).ampm;

        if(hours.length<2){
            hours = "0"+hours;
        }
        if(minutes.length<2){
            minutes = "0"+minutes;
        }

        var finalDateString = hours+":"+minutes+" "+ampm+" - "+weekDay+", "+month+" "+day;

        var rowList = Titanium.UI.createTableViewRow({
            height:100,
            obj:events[i],
            latitude: events[i].get("location").latitude,
            longitude: events[i].get("location").longitude
        });

        var iconCat = Titanium.UI.createImageView({
            left:"3%",
            top:"25%",
            image:"/images/"+api.filtersIcon[events[i].get("category")],
        });

        var eventName = Titanium.UI.createLabel({
            text:events[i].get("name").toUpperCase(),
            top:"17.5%",color:'#20acb9',
            font:{fontFamily:"Raleway-SemiBold",fontSize:18},
            left:'18%',width:"87%",height:23
        });

        var locationIcon = Titanium.UI.createView({
            width:13,height:16,
            backgroundImage:"/images/ic_7_location.png",
            top:"47.5%",left:'18%'
        });

        var eventAddress = Titanium.UI.createLabel({
            text:events[i].get("address"),top:"47%",
            font:{fontFamily:"Raleway-SemiBold",fontSize:14},
            color:'white',left:'24%',
            width:"76%",height:18,
            verticalAlign:Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER
        });

        var dateIcon = Titanium.UI.createView({
            width:14,height:14,
            backgroundImage:"/images/ic_7_time.png",
            top:"68%",left:'17.8%'
        });

        var eventDate = Titanium.UI.createLabel({
            text:finalDateString,
            top:"62%",color:'white',left:'24%',
            font:{fontFamily:"Raleway-Regular",fontSize:12},
            width:"76%",height:30
        });

        var stroke = Titanium.UI.createView({
            backgroundImage:"stroke.png",
            width:"90%",height:0.5,bottom:"0%",right:0
        });

        rowList.add(iconCat);
        rowList.add(eventName);
        rowList.add(locationIcon);
        rowList.add(eventAddress);
        rowList.add(dateIcon);
        rowList.add(eventDate);
        rowList.add(stroke);

        data.push(rowList);
    }; 

    listEvents.setData(data,{animated:true});

    w.add(listEvents);
	return w;
};

module.exports = chooseevent;
