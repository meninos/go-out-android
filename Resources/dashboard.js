function dashboard(){
	
	var data = [];
	var events = null;
	var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

	var w = Ti.UI.createWindow({
		backgroundImage:"/images/bg.jpg",
		exitOnClose:true
	});

	var MapModule = require('ti.map');
	var map = MapModule.createView({
		mapType:MapModule.NORMAL_TYPE,userLocation:true,clicksource:"map",
	});
	w.add(map);

	map.addEventListener("click", function(e){
        if(e.clicksource == 'subtitle') {
        	progressIndicator.setMessage("Loading...");
        	progressIndicator.show();
            if(e.annotation.multiEvents){
                var chooseW = require("chooseevent")(e.annotation.multiEvents);
                chooseW.open();
            }else{
                var profileW = require("eventprofile")(e.annotation.evt);
                profileW.open();
            }
        }
    });

	w.addEventListener("focus",function(e){
        Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
        if(api.isCurrentLocation){
            Titanium.Geolocation.getCurrentPosition(function(e){
                if (!e.success || e.error){
                    alert("Error getting your current location.");
                    progressIndicator.hide();
                    return;
                }

                api.currentLng = e.coords.longitude;
                api.currentLat = e.coords.latitude;

                progressIndicator.setMessage("Loading...");
                progressIndicator.show();
                api.searchAPI(api.currentLat+","+api.currentLng,api.getLocationAddress,true);
                api.returnEventsNearby(showEventsMap);
            });       
        }else{
        	progressIndicator.setMessage("Loading...");
        	progressIndicator.show();
            api.searchAPI(api.currentLat+","+api.currentLng,api.getLocationAddress,true);
            api.returnEventsNearby(showEventsMap);
        }
    });

	function showEventsMap(result,error){
		progressIndicator.hide();
		map.setRegion({
			latitude: api.currentLat,
			longitude: api.currentLng,
			latitudeDelta:0.10, 
			longitudeDelta:0.10,
		});

		if(api.newDate){
	        data = [];
	        events = null;
	        map.removeAllAnnotations();
	        api.newDate = false;
	    }

	    var sortedArray;
	    if(!result){
	        map.removeAllAnnotations();
	        data = [];
	        return;
	    }else{
	    	var resultLength = result.length;
	        sortedArray = [];
	        for(var l = 0; l < resultLength; l++){
	            if(sortedArray.length > 0){
	                var added = false;
	                for(var j = 0; j < sortedArray.length; j++){
	                    if(sortedArray[j].length){
	                        if(result[l].get("location").latitude == sortedArray[j][0].get("location").latitude && result[l].get("location").longitude == sortedArray[j][0].get("location").longitude){
	                            if(sortedArray[j].length < 2){
	                                sortedArray[j] = [sortedArray[j],result[l]];
	                            }else{
	                                sortedArray[j].push(result[l]);
	                            }
	                            added = true;
	                        }
	                    }else{
	                        if(result[l].get("location").latitude == sortedArray[j].get("location").latitude && result[l].get("location").longitude == sortedArray[j].get("location").longitude){
	                            sortedArray[j] = [sortedArray[j],result[l]];
	                            added = true;
	                        }
	                    }
	                }
	                if(!added){
	                    sortedArray.push(result[l]);    
	                }
	            }else{
	                sortedArray.push(result[l]);
	            }
	        }
	    }

	    events = sortedArray;
	    //var eventsLength = events.length;
	    
	    for(var i = 0; i < events.length; i++){
	        var totalEvents = [];
	        
	        if(events[i].length > 1){
	            totalEvents = events[i];
	            events[i] = events[i][0];
	        }

	        var eventDateObj = events[i].get("date");

	        var day = (eventDateObj.getDate()<10) ? "0"+eventDateObj.getDate() : eventDateObj.getDate();
	        var month = months[eventDateObj.getMonth()];
	        var weekDay = days[ eventDateObj.getDay() ];
	        var year = eventDateObj.getFullYear();
	        var hours = ""+ui.formatAMPM(eventDateObj).hour;
	        var minutes = ""+eventDateObj.getMinutes();
	        var ampm = ""+ui.formatAMPM(eventDateObj).ampm;

	        if(hours.length<2){
	            hours = "0"+hours;
	        }
	        if(minutes.length<2){
	            minutes = "0"+minutes;
	        }

	        var finalDateString = hours+":"+minutes+" "+ampm+" - "+weekDay+", "+month+" "+day;

	        var pinImage = Titanium.UI.createView({
	            backgroundImage:"/images/"+ui.filters[events[i].get("category")],
	            width:39,height:56
	        });

	        var testView = Titanium.UI.createView({
	            backgroundColor:"#20acb9",
	            width:30,
	            height:30,
	            right:-10,
	            top:-10,
	            borderRadius:15,
	            borderWidth:3,
	            borderColor:"white",
	        });
	        var labelIndicator = Titanium.UI.createLabel({
	            text:"12",
	            color:"white",
	            font:{
	                fontFamily:"Raleway-Bold",
	                fontSize:16
	            },
	            top:4,
	            textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
	        });

	        var eventPin = Map.createAnnotation({
	            latitude: events[i].get("location").latitude,
	            longitude: events[i].get("location").longitude,
	            originalLatitude: events[i].get("location").latitude,
	            originalLongitude: events[i].get("location").longitude,
	            title: events[i].get("name")+" - "+ui.calculateDistance(events[i].get("location").latitude,events[i].get("location").longitude,api.currentLat,api.currentLng,"N")+" miles",
	            subtitle: finalDateString,
	            pincolor: Map.ANNOTATION_RED,
	            draggable:false,
	            centerOffset: {x: -0, y: -25},  
	            animate:false,
	            evt:events[i],
	            //rightButton:Titanium.UI.iPhone.SystemButton.DISCLOSURE,
	            customView:pinImage,
	        });

	        if(totalEvents.length>0){
	            eventPin.setTitle(events[i].get("name")+" - " + totalEvents.length + " events");
	            eventPin.multiEvents = totalEvents;
	        }

	        var isNew = true;
	        
	        if(map.annotations && map.annotations.length>0){
	            map.annotations.forEach(function(e){
	                if(e.evt.id == events[i].id){
	                    isNew = false;
	                }
	            });
	            if(isNew){
	                map.addAnnotation(eventPin);
	            }
	        }else{
	            map.addAnnotation(eventPin);
	        }        
	    };

	    if(map.annotations && map.annotations.length>0 && events.length>0){
	        map.annotations.forEach(function(e){
	            var toBeDeleted = true;
	            events.forEach(function(ev){
	                if(ev.id == e.evt.id){
	                    toBeDeleted = false;
	                }
	            });
	            if(toBeDeleted){
	                map.removeAnnotation(e);
	            }
	        });
	    }
	}


	return w;
}
module.exports = dashboard;