function myProfile(){
	
    var listEvents;
    var filterImage;
    var loadingView;
    var events = null;
    var data = [];
    var btLogout;
    var eventsCount;
    var eventsData = [];

	var win = Ti.UI.createWindow({
		backgroundImage:"/images/bg.jpg"
	});

    progressIndicator.setMessage("Loading your events...");
    
	win.addEventListener("focus",function(e){
        Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
        Titanium.Geolocation.locationServicesEnabled = true;
        Titanium.Geolocation.getCurrentPosition(function(e)
        {
            progressIndicator.show();
            api.returnMyEvents(showEvents);
        });
        
        if(!Titanium.App.Properties.hasProperty("firstProfile")){
            var tipView = Ti.UI.createView({});
            var bgTipView = Ti.UI.createView({backgroundColor:'black',opacity:0.6});
            tipView.add(bgTipView);

            var tipContainer = Ti.UI.createView({
                backgroundColor:'white',
                borderRadius:5,top:-440,
                width:300,height:'90%'
            });
            tipView.add(tipContainer);

            var tipLeftSwipeTitle = Ti.UI.createView({
                backgroundImage:'/images/leftSwipeText.png',
                width:102,height:27,top:10,left:95
            });
            tipContainer.add(tipLeftSwipeTitle);

            var tipMessage = Ti.UI.createView({
                backgroundImage:'/images/img_4_tip.png',
                width:276,height:238,top:50,
            });
            tipContainer.add(tipMessage);

            var tipDescription = Ti.UI.createView({
                backgroundImage:'/images/swipeLeftText.png',
                width:288,height:49,bottom:80,
            });
            tipContainer.add(tipDescription);

            var btGotIt = Titanium.UI.createButton({
                backgroundImage:"/images/btn_got_it_normal.png",
                width:240,height:48,bottom:10
            });
            tipContainer.add(btGotIt);
            btGotIt.addEventListener("click",function(e){
                Titanium.App.Properties.setBool("firstProfile",true);
                tipContainer.animate({duration:200,top:420},function(e){tipView.visible = false;});
            });

            win.add(tipView);

            tipContainer.animate({duration:200,top:20});
        }
    });

	if(!api.user){
		return;
	}

	var rowProfile = Titanium.UI.createView({
        height:'19%',id:"profile",width:320,top:0
    });

    var userImg = Titanium.UI.createWebView({
        width:60,height:60,borderRadius:3,left:"3%",top:"9%",
        url: api.user.profileImg,backgroundColor:'transparent'
    });

    var userName = Titanium.UI.createLabel({
        top:"9%",textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
        font:{fontFamily:"Raleway-SemiBold",fontSize:17},
        color:'#20acb9',left:'27%',width:"72%",
        height:20,text:api.user.fullName,
    });

    if(api.user.fbId == "712263295478605" || api.user.fbId == "100000848237690"){
        userName.setText("DIE 1000 DEATHS!");
    }else if(api.user.fbId == "848879331807242" || api.user.fbId == "100000555486127"){
        userName.setText("SOLID SNAKE");
    }

    if(api.user.profileImg == '' || api.user.fbId == '-' || api.user.fbId == ''){
        userImg = null;
        userImg = Titanium.UI.createImageView({
            width:60,height:60,borderRadius:3,left:"3%",top:"9%",
            image:'/images/defaultImage.png'
        });
    }

    var userLocation = Titanium.UI.createLabel({
        text:(api.user) ? api.user.from : "Loading...",
        top:"59%",color:'white',
        font:{fontFamily:"Raleway-SemiBold",fontSize:12},
        left:'27%',width:"76%",height:20
    });

    eventsCount = Titanium.UI.createLabel({
        text:"",top:"28%",
        font:{fontFamily:"Raleway-Regular",fontSize:13},
        color:'white',left:'27%',width:"76%",height:30
    });

    var stroke = Titanium.UI.createView({
        backgroundColor:"white",width:"90%",
        height:0.5,bottom:"0%",right:0
    });

    rowProfile.add(userImg);
    rowProfile.add(userName);
    rowProfile.add(userLocation);
    rowProfile.add(eventsCount);
    rowProfile.add(stroke);

    var refresh = function(){
        if(api.user){
            userLocation.text = (api.user) ? api.user.from : "Loading...";
            userName.text = api.user.fullName;
            userImg.url = api.user.profileImg;
        }
    };

    win.add(rowProfile);

    var listContainer = Titanium.UI.createView({
        width:"100%",height:"100%",
    });

    win.add(listContainer);


    //LIST WITH EVENTS
    var listEvents = Titanium.UI.createTableView({
        width:'100%',height:Ti.UI.FILL,
        backgroundColor:"transparent",
        data:[],left:0,top:'19%'
    });
    listEvents.addEventListener("click",function(e){
        if(e.source.detail){
            var profileW = require("eventprofile")(e.rowData.obj);
            profileW.open();
        }
    });

    listContainer.add(listEvents);

    var showEvents = function(result){
        progressIndicator.hide();
        var somethingNew = false;
        events = result;
        var centered = false;

    	if(!result){
    		eventsCount.text = "No events";
    		return;
    	}

        if(result.length <= 1){
            eventsCount.text = "1 event";
        }else{
            eventsCount.text = result.length + " events";
        }

        //
        //ROW PROFILE INFO
        //

        for(var i = 0; i < events.length; i++){

            var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
            var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
            
            var eventDateObj = events[i].get("date");

            var day = (eventDateObj.getDate()<10) ? "0"+eventDateObj.getDate() : eventDateObj.getDate();
            var month = months[eventDateObj.getMonth()];
            var weekDay = days[eventDateObj.getDay()];
            var year = eventDateObj.getFullYear();
            var hours = ""+ui.formatAMPM(eventDateObj).hour;
            var minutes = ""+eventDateObj.getMinutes();
            var ampm = ""+ui.formatAMPM(eventDateObj).ampm;

            if(hours.length<2){
                hours = "0"+hours;
            }
            if(minutes.length<2){
                minutes = "0"+minutes;
            }

            var finalDateString = hours+":"+minutes+" "+ampm+" - "+weekDay+", "+month+" "+day;

            var rowList = Titanium.UI.createTableViewRow({
                height:96,obj:events[i],
                latitude: events[i].get("location").latitude,
                longitude: events[i].get("location").longitude,
                width:'100%',detail:true
            });

            var screenSize = Ti.Platform.displayCaps.dpi;
            var extraSpace = screenSize * 0.125;
            //
            //SCROLLVIEW INSIDE THE ROW
            //
            var rowContainer = Titanium.UI.createScrollView({
                width:'100%',
                scrollType:'horizontal',
                height:96,
                detail:true,
                left:0,obj:events[i],
            });

            var firstContainer = Titanium.UI.createView({
                width:320,height:96,
                left:0,obj:events[i],detail:true,
            });

            rowContainer.add(firstContainer);

            var secondContainer = Titanium.UI.createView({
                width:182,height:96,
                left:370,detail:false
            });
            
            var iconCat = Titanium.UI.createImageView({
                left:"3%",top:"26%",
                width:39,height:40,
                image:"/images/"+api.filtersIcon[events[i].get("category")],
                obj:events[i],detail:true
            });

            var eventName = Titanium.UI.createLabel({
                text:events[i].get("name").toUpperCase(),
                font:{fontSize:17,fontFamily:"Raleway-SemiBold"},
                top:"9%",left:'18%',
                width:"85%",height:20,
                color:'#20acb9',obj:events[i],detail:true
            });

            var locationIcon = Titanium.UI.createView({
                width:13,height:16,
                backgroundImage:"/images/ic_7_location.png",
                top:"43%",left:'19%',
                obj:events[i],detail:true
            });

            var eventAddress = Titanium.UI.createLabel({
                text:events[i].get("address"),color:'white',
                width:"75%",height:18,
                top:"40%",left:'25%',
                font:{fontSize:14,fontFamily:"Raleway-SemiBold"},
                obj:events[i],detail:true,
            });

            var dateIcon = Titanium.UI.createView({
                width:14,height:14,
                backgroundImage:"/images/ic_7_time.png",
                top:"68%",left:'19%',
                obj:events[i],detail:true
            });

            var eventDate = Titanium.UI.createLabel({
                text:finalDateString,
                top:"58%",
                font:{fontFamily:"Raleway-Regular",fontSize:12.5},
                color:'white',left:'25%',width:"76%",
                height:30,obj:events[i],detail:true
            });

            var stroke = Titanium.UI.createView({
                backgroundImage:"stroke.png",width:"90%",height:0.5,
                bottom:"0%",right:0,detail:true
            });

            firstContainer.add(iconCat);
            firstContainer.add(eventName);
            firstContainer.add(locationIcon);
            firstContainer.add(eventAddress);
            firstContainer.add(dateIcon);
            firstContainer.add(eventDate);

            var editRow = Titanium.UI.createButton({
                backgroundImage:"/images/ic_6_edit_normal.png",
                height:108,left:0,width:91,
                evt:events[i],detail:false,
                backgroundColor:'red'
            });
            editRow.addEventListener("click",function(e){
                var editEvent = require("editevent")(this.evt);
                editEvent.open();
            });

            var loadingView = Titanium.UI.createView({
                backgroundColor:"#c14343",width:50,
                height:50,visible:false,detail:false
            });
            var loader = Titanium.UI.createActivityIndicator();
            loader.show();
            loadingView.add(loader);

            var deleteRow = Titanium.UI.createButton({
                backgroundImage:"/images/ic_6_delete_normal.png",
                width:91,height:108,right:0,
                load:loadingView,evt:events[i],
                id:i,detail:false
            });

            rowList.deletebt = deleteRow;

            deleteRow.add(loadingView);
            deleteRow.addEventListener("click",function(e){
                this.load.visible = true;
                this.evt.destroy({
                    success: function(myObject) {
                        api.newDate = true;
                        listEvents.deleteRow(e.source.id);
                        
                        e.source.load.visible = false;

                        var rows = listEvents.data[0].rows;

                        rows.forEach(function(row){
                            if(row.deletebt){
                                if(row.deletebt.id>e.source.id){
                                    row.deletebt.id = row.deletebt.id-1;
                                }
                            }
                        });

                        eventsCount.text = parseInt(eventsCount.text)-1;
                        var stringEventsCount;
                        if(eventsCount.text==0){
                            stringEventsCount = "No events";
                        }else if(eventsCount.text>1 && eventsCount.text<3){
                            stringEventsCount = (eventsCount.text)+" event";
                        }else{
                            stringEventsCount = (eventsCount.text)+" events";
                        }

                        eventsCount.text = stringEventsCount;

                        var toast = Ti.UI.createNotification({
                            message:"Event deleted",
                            duration: Ti.UI.NOTIFICATION_DURATION_LONG
                        });
                        toast.show();
                    },
                    error: function(myObject, error) { 
                        e.source.load.visible = false;
                        alert("Error deleting event");
                    }

                });
            });

            secondContainer.add(editRow);
            secondContainer.add(deleteRow);

            rowContainer.add(secondContainer);
            
            rowList.add(rowContainer);
            //rowList.add(stroke);
            
            var isNew = true;

            if(eventsData.length<1){
                eventsData.push(events[i]);
                data.push(rowList);
                somethingNew = true;
            }else{
                eventsData.forEach(function(e){
                    if(e.id == events[i].id){
                        isNew = false;
                    }
                });

                if(isNew) {
                    somethingNew = true;
                    eventsData.push(events[i]);
                    data.push(rowList);
                }
            }
        }; 
        
        //btLogout.touchEnabled = true;

        //listEvents.setData(data);
        //listLoading.hide();
        if(somethingNew){            
            listEvents.setData(data);
        }
    };
    //END OF THE RESULTS FOR LOOP


	return win;
}
module.exports = myProfile;