
var listEvents;
var filterImage;
var events = null;
var data = [];

var eventsData = [];

function list(){

	var win = Ti.UI.createWindow({
        backgroundImage:"/images/bg.jpg"
    });

    win.addEventListener("focus",function(e){
        progressIndicator.setMessage("Loading events...");
        Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
        progressIndicator.show();
        if(api.isCurrentLocation){
            Titanium.Geolocation.getCurrentPosition(function(e)
            {
                if (!e.success || e.error)
                {
                    alert("Error getting your current location.");
                    progressIndicator.hide();
                    return;
                }

                api.currentLng = e.coords.longitude;
                api.currentLat = e.coords.latitude;

                api.searchAPI(api.currentLat+","+api.currentLng,api.getLocationAddress,true);
                api.returnEventsNearby(showEvents);
            });       
        }else{
            api.searchAPI(api.currentLat+","+api.currentLng,api.getLocationAddress,true);
            api.returnEventsNearby(showEvents);
        }
    });

    //
    //SEARCH BAR
    //
    var bgView = Titanium.UI.createView({
        top:0,
        height:45,
        backgroundColor:"#35353b"
    });

    var textFieldSearch = Titanium.UI.createTextField({
        height:35,
        width:"90%",
        borderRadius:5,
        hintText:"Search for Events",   
        font:{
            fontSize:15
        },
        textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
        color:"white"
    });

    bgView.add(textFieldSearch);

    listEvents = Titanium.UI.createTableView({
        width:Ti.UI.FILL,
        height:Ti.UI.FILL,
        backgroundColor:"transparent",
        data:[],left:0,top:45
    });
    listEvents.addEventListener("click",function(e){
        var profileW = require("eventprofile")(e.rowData.obj);
        profileW.open();
    });

    textFieldSearch.addEventListener("change",function(e){
        filtered = [];
        if(data.length > 0){
            data.forEach(function(data){
                if(data.obj.get("name").lastIndexOf(e.value, 0) === 0){
                    filtered.push(data);
                }  
            });
        }
        
        listEvents.setData(filtered,{animated:true});
    });

    win.add(bgView);
    win.add(listEvents);

    var showEvents = function(result){
    	progressIndicator.hide();
        var somethingNew = false;

        if(!result){
            listEvents.setData([],{animated:true});
            data = [];
            eventsData = [];
            return;
        }
        events = result;
        data = [];
        eventsData = [];

        for(var i = 0;i<events.length;i++){

            var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
            var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

            var eventDateObj = events[i].get("date");

            var day = (eventDateObj.getDate()<10) ? "0"+eventDateObj.getDate() : eventDateObj.getDate();
            var weekDay = days[ eventDateObj.getDay() ];
            var month = months[eventDateObj.getMonth()];
            var year = eventDateObj.getFullYear();
            var hours = ""+ui.formatAMPM(eventDateObj).hour;
            var minutes = ""+eventDateObj.getMinutes();
            var ampm = ""+ui.formatAMPM(eventDateObj).ampm;

            if(hours.length<2){
                hours = "0"+hours;
            }
            if(minutes.length<2){
                minutes = "0"+minutes;
            }

            var finalDateString = hours+":"+minutes+" "+ampm+" - "+weekDay+", "+month+" "+day;

            var rowList = Titanium.UI.createTableViewRow({
                height:100,
                obj:events[i],
                latitude: events[i].get("location").latitude,
                longitude: events[i].get("location").longitude
            });

            var iconCat = Titanium.UI.createImageView({
                left:"4.5%",
                image:"/images/"+api.filtersIcon[events[i].get("category")],
                width:40,
                height:40
            });

            var eventName = Titanium.UI.createLabel({
                text:events[i].get("name").toUpperCase(),
                top:"17.5%",
                font:{
                    fontFamily:"Raleway-SemiBold",
                    fontSize:19
                },
                color:'#20acb9',
                left:'21%',
                width:"82%",
                height:20
            });

            var locationIcon = Titanium.UI.createView({
                width:11,
                height:14,
                backgroundImage:"locationicon.png",
                top:"47.5%",
                left:'18%'
            });

            var eventAddress = Titanium.UI.createLabel({
                text:events[i].get("address"),
                top:"45%",
                font:{
                    fontFamily:"Raleway-SemiBold",
                    fontSize:14
                },
                color:'white',
                left:'21%',
                width:"76%",
                height:18
            });

            var dateIcon = Titanium.UI.createView({
                width:14,
                height:14,
                backgroundImage:"icontime.png",
                top:"71%.5%",
                left:'17.8%'
            });

            var eventDate = Titanium.UI.createLabel({
                text:finalDateString,
                top:"62%",
                font:{
                    fontFamily:"Raleway-Regular",
                    fontSize:12.5
                },
                color:'white',
                left:'21%',
                width:"76%",
                height:30
            });

            var stroke = Titanium.UI.createView({
                backgroundColor:"white",
                width:"100%",
                height:0.5,
                bottom:"0%",
                right:0,
                opacity:0.3
            });

            rowList.add(iconCat);
            rowList.add(eventName);
            rowList.add(locationIcon);
            rowList.add(eventAddress);
            rowList.add(dateIcon);
            rowList.add(eventDate);
            rowList.add(stroke);
                
            var isNew = true;

            if(eventsData.length<1){
                eventsData.push(events[i]);
                data.push(rowList);
                somethingNew = true;
            }else{
                eventsData.forEach(function(e){
                    if(e.id == events[i].id){
                        isNew = false;
                    }
                });

                if(isNew) {
                    somethingNew = true;
                    eventsData.push(events[i]);
                    data.push(rowList);
                }
            }
        }; 

        api.loadedOncelist = true;

        //
        //DELETE IF DOESNT EXIST ANYMORE
        //    
        var rowsDontDelete = [];
        for(var d=0 ; d<data.length ; d++){
            
            var deleteRow = true;
            
            for(var e = 0 ; e<events.length ; e++){
                if(data[d].obj.id == events[e].id){
                    deleteRow = false;
                }
            }

            if(!deleteRow){
                rowsDontDelete.push(data[d]);
                somethingNew = true;
            }
        }
        if(somethingNew){
            listEvents.setData(rowsDontDelete,{animated:true});
        }

        eventsData = [];
        for(var l = 0;l<events.length;l++){
            eventsData.push(events[l]);
        }
    };

	return win;
}
module.exports = list;