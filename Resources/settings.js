function settings(){
	var w = Ti.UI.createWindow({
        backgroundImage:"/images/bg.jpg"
    });

    var customFont = 'Raleway';

    var distanceLabel = Titanium.UI.createLabel({
        text:"Distance",top:"5.5%",left:'6%',color:'white',
        font:{fontFamily:"Raleway-SemiBold",fontSize:19},
    });

    var distanceNumber = Titanium.UI.createLabel({
        text:5+"mi",top:"6%",right:'6%',color:'white',
        font:{fontFamily:"Raleway-SemiBold",fontSize:19},
    });

    var sliderDistance = Titanium.UI.createSlider({
        width:'90%',top:"14%",min:1,max:1000,
        value:Titanium.App.Properties.getInt("distance",null),
        thumbImage:"/images/sliderball.png",
    });

    sliderDistance.addEventListener("change",function(e){
        var newValue = parseValue(e.value,1);
        distanceNumber.text = newValue+"mi";
        Titanium.App.Properties.setInt("distance",newValue);
    });


    var notificationLabel = Titanium.UI.createLabel({
        text:"Notifications",top:"26%",color:'white',left:'6%',
        font:{fontFamily:"Raleway-SemiBold",fontSize:19},
    });

    var btNo = Titanium.UI.createButton({
        right:"28%",top:"25.3%",
        backgroundImage:'/images/settingsBtNo.png',
        oImage:'/images/settingsBtNo.png',
        sImage:'/images/settingsBtNoS.png',
        width:60,height:31,id:1,
    });
    btNo.addEventListener("click",function(e){
        notificationHandler(this.id);
    });

    var btYes = Titanium.UI.createButton({
        top:"25.3%",right:"6%",
        backgroundImage:'/images/settingsBtYesS.png',
        oImage:'/images/settingsBtYes.png',
        sImage:'/images/settingsBtYesS.png',
        width:60,height:31,id:2,
    });
    btYes.addEventListener("click",function(e){
        notificationHandler(this.id);
    });

    function notificationHandler(id){
	    if(id==1){
	        btNo.backgroundImage = btNo.sImage;
	        btYes.backgroundImage = btYes.oImage;
	        //api.unSubscribeDeviceToPush();
	    }else{
	        btNo.backgroundImage = btNo.oImage;
	        btYes.backgroundImage = btYes.sImage;
	        //api.subscribeDeviceToPush();
	    }
	};

    var dateLabel = Titanium.UI.createLabel({
        text:"Events for",color:'white',
        top:"38%",left:'6%',
        font:{fontFamily:"Raleway-SemiBold",fontSize:19},
    });

    var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

    var setDay = new Date();

    var currentDay = setDay.getDate();
    var weekDay = days[setDay.getDay()];

    setDay.setDate(setDay.getDate()+Titanium.App.Properties.getInt("days",null));

    var day = (setDay.getDate()<10) ? "0"+setDay.getDate() : setDay.getDate();
    var month = months[setDay.getMonth()];
    var year = setDay.getFullYear();

    var dateNumber = Titanium.UI.createLabel({
        text: (Titanium.App.Properties.getInt("days",null) == 0) ? "Today" : weekDay+", "+month+" "+day,
        top:"39%",color:'#20acb9',right:'6%',
        font:{fontFamily:"Raleway-SemiBold",fontSize:16},
    });


    var sliderText = new Date();

    var sliderDays = Titanium.UI.createSlider({
        top:"45%",width:'90%',
        min:0,max:60,
        thumbImage:"/images/sliderball.png",
        value:Titanium.App.Properties.getInt("days",null)
    });

    sliderDays.addEventListener("change",function(e){
        e.value = Math.round(e.value);
        
        var today = new Date();
        var otherDay = new Date();
        otherDay.setDate(today.getDate()+e.value);

        Titanium.App.Properties.setInt("days",e.value);

        var day = (otherDay.getDate()<10) ? otherDay.getDate() : otherDay.getDate();
        weekDay = days[otherDay.getDay()];
        var month = months[otherDay.getMonth()];
        var year = otherDay.getFullYear();
        
        dateNumber.text = (e.value == 0) ? "Today" : weekDay+", "+month+" "+day;

        api.newDate = true;
    });

    var contactButtonContainer = Ti.UI.createView({
        backgroundColor:'transparent',bottom:'20%',width:200,height:40
    });
    var contactUs = Titanium.UI.createButton({
        backgroundImage:"/images/contact.png",
        width:185,height:17,
    });
    contactButtonContainer.add(contactUs);
    contactButtonContainer.addEventListener('click',function(e){
        var emailDialog = Ti.UI.createEmailDialog();
        if(emailDialog.isSupported()){
            emailDialog.subject = "Contact Us";
            emailDialog.toRecipients = ["teamgoout@gmail.com"];
            emailDialog.messageBody = "Use this space to suggest new features, report bugs, or just to say something for us";
            emailDialog.open();
        }else{
            ui.alert("No E-mail configured on this Device.\n You can configure a new one on Settings.");
        }
    });

    var termsButtonContainer = Ti.UI.createView({
        backgroundColor:'transparent',bottom:'10%',width:200,height:40
    });

    var terms = Titanium.UI.createButton({
        backgroundImage:"/images/terms.png",
        width:169,height:16,
    });
    termsButtonContainer.add(terms);
    termsButtonContainer.addEventListener("click",function(e){
        var termsWindow = require("terms")();
        termsWindow.open();
    });

    w.add(distanceLabel);
    w.add(distanceNumber);
    w.add(sliderDistance);
    w.add(notificationLabel);
    w.add(btNo);
    w.add(btYes);
    w.add(dateLabel);
    w.add(dateNumber);
    w.add(sliderDays);
    w.add(contactButtonContainer);
    w.add(termsButtonContainer);
	
	return w;
}
module.exports = settings;