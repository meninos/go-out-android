function eventprofile(e){
	var evt = e;

	console.log(evt);
	
	var w = Ti.UI.createWindow({
		theme:"Theme.AppCompat.Translucent.NoTitleBar",
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
		backgroundImage:"/images/bg.jpg"
	});

    w.addEventListener('open',function(e){
        progressIndicator.hide();
    });

	var topView = ui.view({
        backgroundColor:'#2b2831',
        width:'100%',height:'10%',
        top:0,left:0
    });

    var eventDetailsTitle = Titanium.UI.createView({
        backgroundImage:'/images/evtDetailsTitle.png',
        width:124,height:14,left:'2%',
        backgroundColor:'transparent'
    });
    topView.add(eventDetailsTitle);
    eventDetailsTitle.addEventListener('click',function(e){w.close();});

    var share = Titanium.UI.createButton({
        backgroundImage:"/images/ic_7_share_normal.png",
        width:23,height:23,right:'5%',
    });
    topView.add(share);

    var eventDate = new Date(evt.attributes.date);
    var eventDay = eventDate.getDate();
    var eventMonth = eventDate.getMonth()+1;
    var eventYear = eventDate.getFullYear();

    if(eventDay < 10){
        eventDay = '0' + eventDay;
    }

    if(eventMonth < 10){
        eventMonth = '0' + eventMonth;
    }

    share.addEventListener("click",function(e){

        var dialog = Titanium.UI.createOptionDialog({
            title: 'Share Event Using...',
            options: ['Twitter','Facebook', 'E-mail', 'SMS'],
            destructive:2,
        });
        dialog.show();
        dialog.addEventListener('click', function(e) {
            if(e.index == 0) {
                //TWITTER SHARE
                try{
                    var intTwitter = Ti.Android.createIntent({
                        action: Ti.Android.ACTION_SEND,
                        packageName: "com.twitter.android",
                        flags: Ti.Android.FLAG_ACTIVITY_NEW_TASK,
                        type: "text/plain"
                    });
                    var twitterMessage = 'Save the date: ' + eventMonth+'/'+eventDay+'/'+eventYear + ' - ' + evt.attributes.name;
                    intTwitter.putExtra(Ti.Android.EXTRA_TEXT, twitterMessage);
                    Ti.Android.currentActivity.startActivity(intTwitter);
                } catch(x) {
                    Ti.Platform.openURL("market://details?id=com.twitter.android");
                }
            }else if(e.index == 1){
                //FACEBOOK SHARE
                var data = {
                    link : "https://itunes.apple.com/us/app/go-out/id910395697?mt=8",
                    name : "Go Out",
                    caption : "Hey,! \n I'm using Go Out and I want to invite you to this event: " + evt.attributes.name,
                    description : "At: " + evt.attributes.address,
                };
                fb.dialog("feed", data, function(e) {
                    if (e.success && e.result) {
                        //alert('Success!');
                    } else {
                        alert('Error posting on your Facebook Timeline');
                    }
                });
            }
            else if(e.index == 2){
                var body = "Hey,! <br>I'm using Go Out and I want to invite you to this event: <br><b>" + evt.attributes.name + "</b><br><br>At: <br>" + evt.attributes.address;
                body += "<br><br> " + evt.attributes.description;
                var emailDialog = Ti.UI.createEmailDialog({
                    subject:"New Go Out Event!",
                    messageBody:body,
                    html:true
                });
                emailDialog.open();
            }else if(e.index == 3){
                var messageBody = "Hey, \nI'm using Go Out and I want to invite you to this event: " + evt.attributes.name + '\nAt: ' + evt.attributes.address;
                var intent = Ti.Android.createIntent({
                    action: Ti.Android.ACTION_VIEW,type: 'vnd.android-dir/mms-sms'
                });
                intent.putExtra('sms_body', messageBody);
                Ti.Android.currentActivity.startActivityForResult(intent, function(e){});
            }
        });
    });

    w.add(topView);

    var v = Ti.UI.createScrollView({
        width:"100%",height:"92%",top:50,
    });
    w.add(v);

    var Map = require('ti.map');
    //
    //MAP VIEW
    //
    var map = Map.createView({
        width:"100%",top:0,height:220,
        userLocation:true,
    });
    v.add(map);

    var eventPin = Map.createAnnotation({
        latitude: evt.attributes.location.latitude,
        longitude: evt.attributes.location.longitude,
        title: evt.attributes.name,
        subtitle:evt.attributes.address,
        draggable:false,
        centerOffset: {x: -0, y: -25},
        image:"/images/"+ui.filters[evt.attributes.category]
    });

    map.setRegion({
        latitude : evt.attributes.location.latitude+0.0180,
        longitude : evt.attributes.location.longitude,
        latitudeDelta : 0.10,
        longitudeDelta : 0.10
    });

    map.addAnnotation(eventPin);
    setTimeout(function(){
        map.selectAnnotation(eventPin);    
    },500);
    
    var eventNameLabel = Titanium.UI.createLabel({
        text:evt.attributes.name.toUpperCase(),
        color:'#20acb9',width:Ti.UI.SIZE,
        height:20,top:240,
        textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
        font:{fontFamily:"Raleway-SemiBold",fontSize:17},
    });
    v.add(eventNameLabel);

    var containerGroupAddress = Titanium.UI.createView({
        top:280,left:"4%",
        width:'95%',height:Ti.UI.SIZE,
    });

    var locationIcon = Titanium.UI.createView({
        width:13,height:16,
        backgroundImage:"/images/ic_7_location.png",
        top:"8%",left:'0%',
    });

    var eventAddress = Titanium.UI.createLabel({
        text:evt.attributes.address,
        top:"1%",left:'10%',width:"70%",height:45,
        font:{fontFamily:"Raleway-SemiBold",fontSize:13},
        color:'white',
    });

    //DISTANCE
    var eventDistance = Titanium.UI.createLabel({
        text:ui.calculateDistance(evt.attributes.location.latitude,evt.attributes.location.longitude,api.currentLat,api.currentLng,"N")+"mi",
        top:'2%',color:'white',right:10,
        font:{fontFamily:"Raleway-SemiBold",fontSize:15},
        textAlign:Ti.UI.TEXT_ALIGNMENT_RIGHT,
    });

    containerGroupAddress.add(locationIcon);
    containerGroupAddress.add(eventAddress);
    containerGroupAddress.add(eventDistance);

    containerGroupAddress.addEventListener('click',function(e){
        var dialog = Ti.UI.createOptionDialog({
            title:'Navigate using:',
            cancel: 2,
            options: ['Google Maps', 'Waze', 'Cancel'],
        });
        dialog.show();
        dialog.addEventListener('click',function(e){
            switch(e.index){
                case 0:
                    Ti.Platform.openURL('http://maps.google.com/maps?daddr=' + evt.attributes.location.latitude + ',' + evt.attributes.location.longitude);
                break;
    
                case 1:
                    if(!Ti.Platform.openURL('waze://?ll=' + evt.attributes.location.latitude + ',' + evt.attributes.location.longitude + "&navigate=yes")){
                        Ti.Platform.openURL("market://details?id=com.waze");
                    }
                break;
            }
        });
    });

    v.add(containerGroupAddress);

    //
    //DATE GROUP
    //

    var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        
    var eventDateObj = evt.attributes.date;

    var day = (eventDateObj.getDate()<10) ? "0"+eventDateObj.getDate() : eventDateObj.getDate();
    var month = months[eventDateObj.getMonth()];
    var weekDay = days[ eventDateObj.getDay() ];
    var year = eventDateObj.getFullYear();
    var hours = ""+ui.formatAMPM(eventDateObj).hour;
    var minutes = ""+eventDateObj.getMinutes();
    var ampm = ""+ui.formatAMPM(eventDateObj).ampm;

    if(hours.length<2){
        hours = "0"+hours;
    }
    if(minutes.length<2){
        minutes = "0"+minutes;
    }

    var finalDateString = hours+":"+minutes+" "+ampm+" - "+weekDay+", "+month+" "+day;

    //eventPin.subtitle = finalDateString;

    var containerGroupDate = Titanium.UI.createView({
        layout:"horizontal",
        top:320,width:Ti.UI.SIZE,
        height:Ti.UI.SIZE,left:"4%",
    });

    var dateIcon = Titanium.UI.createView({
        backgroundImage:"/images/ic_7_time.png",
        width:16,height:16,
        left:'0%',top:15
    });

    var eventDate = Titanium.UI.createLabel({
        text:finalDateString,
        font:{fontFamily:"Raleway-Regular",fontSize:12},
        color:'white',left:'5%',
        height:30,top:5,width:"75%",
    });

    containerGroupDate.add(dateIcon);
    containerGroupDate.add(eventDate);

    v.add(containerGroupDate);

    var verticalImageAndText = Titanium.UI.createView({
        width:"100%",layout:"vertical",
        height:"auto",top:370,
    });

    var textArea = Titanium.UI.createLabel({
        text:evt.attributes.description,
        backgroundColor:'transparent',top:0,left:"15%",
        font:{fontFamily:"Raleway-Regular",fontSize:16},
        color:'white',width:"82%",height:"auto",
        textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT ,
    });

    verticalImageAndText.add(textArea);

    if(evt.attributes.eventimage){
        if(evt.attributes.eventimage.length > 1){
            var imgContainer = Titanium.UI.createImageView({
                width:300,height:300,
                top:(evt.attributes.description.length > 50) ? 10 : 40,
                image:evt.attributes.eventimage,
            });
            var margin = Ti.UI.createView({
                height:30,top:0
            });
            verticalImageAndText.add(imgContainer);
            verticalImageAndText.add(margin);
        }
    }
    v.add(verticalImageAndText);

	return w;
}

module.exports = eventprofile;