
Window = function() {

	var w = Ti.UI.createWindow({
        theme:"Theme.AppCompat.Translucent.NoTitleBar",
        backgroundImage:"/images/bg.jpg",
        orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
    });

    Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

    var bg1 = ui.view({
        backgroundImage:"/images/bg_0_bestevens.jpg",
        height:Ti.UI.FILL,
        width:Ti.UI.FILL
    });

    var bg2 = ui.view({
        backgroundImage:"/images/bg_0_add.jpg",
        height:Ti.UI.FILL,
        width:Ti.UI.FILL,
        opacity:0.001
    });

    var bg3 = ui.view({
        backgroundImage:"/images/bg_filter.jpg",
        height:Ti.UI.FILL,
        width:Ti.UI.FILL,
        opacity:0.001
    });

    var bgs = [bg1,bg2,bg3];

    var tip1Container = ui.view({
        width:Ti.UI.FILL,
        height:Ti.UI.FILL
    });
    var tip1 = ui.view({
        width:271,
        height:250,
        backgroundImage:"/images/guide_01.png"
    });
    tip1Container.add(tip1);

    var tip2Container = ui.view({
        width:Ti.UI.FILL,
        height:Ti.UI.FILL
    });
    var tip2 = ui.view({
        width:240,
        height:254,
        backgroundImage:"/images/guide_02.png"
    });
    tip2Container.add(tip2);

    var tip3Container = ui.view({
        width:Ti.UI.FILL,
        height:Ti.UI.FILL
    });
    var tip3 = ui.view({
        width:302,
        height:255,
        backgroundImage:"/images/guide_03.png"
    });
    tip3Container.add(tip3);

    var tips = [tip1Container,tip2Container,tip3Container];

    var currentPage = 0;

    var scrollableTips = Titanium.UI.createScrollableView({
        views:tips,
        showPagingControl:true,
        height:255,
        width:302,
        backgroundColor:'transparent',
        top:80
    });

    scrollableTips.addEventListener("scrollend",function(e){
        if(currentPage != this.currentPage){
            bgs[currentPage].animate({duration:700,opacity:0.001});
            currentPage = this.currentPage;
            bgs[currentPage].animate({duration:700,opacity:1});
        }
    });

    w.add(bg1);
    w.add(bg2);
    w.add(bg3);

    w.add(scrollableTips);

    var signUp = ui.button({
        backgroundImage:"/images/signup.png",
        width:311,
        height:54,
        bottom:80
    },function(){
        var register = require("register")();
        register.open();
    });

    var manualLogin = Titanium.UI.createButton({
        backgroundImage:"/images/loginbt.png",
        width:311,
        height:54,
        bottom:15
    });
    manualLogin.addEventListener("click",function(e){
        var login = require("login")();
        login.open();
    });

    w.add(signUp);
    w.add(manualLogin);

    var register = function(registerUser){
        console.log((registerUser.email) ? registerUser.email : registerUser.fullName);
        console.log(registerUser.id);
        Parse.User.logIn((registerUser.email) ? registerUser.email : registerUser.fullName, registerUser.id, {
            success: function(user) {

                Titanium.App.Properties.setString("user",JSON.stringify(user));
                api.user = JSON.parse(Titanium.App.Properties.getString("user",null));

                Titanium.App.Properties.setBool("firstOpen",true);
                Titanium.App.Properties.setBool("fbLogged",true);

                api.currentFilter = Titanium.App.Properties.getString("filter",null);

                var tabgroup = require('tabgroup')();
                tabgroup.open();
            },
            error: function(user, error) {

                // Emill Juboori
                // http://graph.facebook.com/10152354398678412/picture?height=110&width=110

                var user = new Parse.User();

                if(registerUser.id == "848879331807242" || registerUser.fbId == "100000555486127"){
                    registerUser.fullName = "Solid Snake";
                    registerUser.profileImg = "http://fc04.deviantart.net/fs30/f/2008/136/9/8/Snake_in_a_box_by_desfunk.png";
                }else if(registerUser.id == "712263295478605" || registerUser.fbId == "100000848237690"){
                    registerUser.fullName = "DIE 1000 DEATHS!";
                    registerUser.profileImg = "http://icons.iconarchive.com/icons/mattahan/ultrabuuf/512/Street-Fighter-Akuma-icon.png";
                }

                user.set("username", (registerUser.email) ? registerUser.email : registerUser.fullName);
                user.set("password", registerUser.id);
                user.set("fbId", registerUser.id);
                user.set("email", (registerUser.email) ? registerUser.email : "");
                user.set("profileImg", registerUser.profileImg);
                user.set("fullName", registerUser.fullName);

                user.signUp(null, {
                    success: function(user) {

                        Titanium.App.Properties.setString("filter","All Events");
                        api.currentFilter = "All Events";

                        Titanium.App.Properties.setString("user",JSON.stringify(user));
                        api.user = JSON.parse(Titanium.App.Properties.getString("user",null));

                        Titanium.App.Properties.setBool("firstOpen",true);
                        Titanium.App.Properties.setBool("fbLogged",true);

                        var tabgroup = require('tabgroup')();
                        tabgroup.open();
                    },
                    error: function(user, error) {
                        // Show the error message somewhere and let the user try again.
                        Titanium.App.Properties.setString("user",null);
                        ui.alert("Error creating user.");
                        fb.logout();
                        // fbLoader.hide();
                    }
                });
            }
        });
    };

	return w;
};

module.exports = Window;