function tabgroup(){

	Ti.App.tabGroup = Titanium.UI.createTabGroup({
	    navBarHidden:true,
	    orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
        backgroundColor:"313033",
        exitOnClose:true
	});

	Ti.App.tabGroup.addEventListener("open", function(e) {
        var activity = Ti.App.tabGroup.getActivity();
        activity.onCreateOptionsMenu = function(e) {
            var menu = e.menu;
            menu.clear();
            //if (Ti.App.tabGroup.activeTab == tabDashboard) {
                var menuItemFilter = menu.add({
                    icon : "/images/ic_3_filter_normal.png",
                    showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                    width:19,height:21
                });
                menuItemFilter.addEventListener("click", function(e) {
                    var filterWindow = require('filters')();
                    filterWindow.open();
                });

                var menuItemAddEvent = menu.add({
                    icon : "/images/ic_1_add_normal.png",
                    showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                    width:23,height:23
                });
                menuItemAddEvent.addEventListener("click", function(e) {
                    var addEventWindow = require('addEvent')();
                    addEventWindow.open();
                });
                
            //} else if (Ti.App.tabGroup.activeTab == tabList) {
                
            //}
        };
    });
 
    Ti.App.tabGroup.addEventListener("focus", function(e) {
        Ti.App.tabGroup.getActivity().invalidateOptionsMenu();
    });

	var dashboardWindow = require('dashboard')();
	var tabDashboard = Titanium.UI.createTab({  
	    icon:'/images/tab_map_active.png',
	    window:dashboardWindow,
        selectedIcon: "/images/tab_map_active.png",
        unselectedIcon: "/images/tab_map_normal.png",
        backgroundSelectedColor:"#313033",
        backgroundColor:"#313033",
        
	});

	var listWindow = require('list')();
	var tabList = Titanium.UI.createTab({  
	    icon:'/images/tab_list_normal.png',  
	    window:listWindow,
        selectedIcon: "/images/tab_list_active.png",
        unselectedIcon: "/images/tab_list_normal.png",
        backgroundSelectedColor:"#313033",
        backgroundColor:"#313033",
	});

	var myProfileWindow = require('myProfile')();
	var tabMyProfile = Titanium.UI.createTab({  
	    icon:'/images/tab_profile_normal.png',
	    window:myProfileWindow,
        selectedIcon: "/images/tab_profile_active.png",
        unselectedIcon: "/images/tab_profile_normal.png",
        backgroundSelectedColor:"#313033",
        backgroundColor:"#313033",
	});

	var settingsWindow = require('settings')();
	var tabSettings = Titanium.UI.createTab({  
	    icon:'/images/tab_settings_normal.png',
	    window:settingsWindow,
        selectedIcon: "/images/tab_settings_active.png",
        unselectedIcon: "/images/tab_settings_normal.png",
        backgroundSelectedColor:"#313033",
        backgroundColor:"#313033",
	});

	Ti.App.tabGroup.addEventListener('focus', function(e){
        for(var i = 0 ; i<Ti.App.tabGroup.tabs.length ; i++){
            if(i == e.index){
                Ti.App.tabGroup.tabs[i].icon = Ti.App.tabGroup.tabs[i].selectedIcon;
            }else{
                Ti.App.tabGroup.tabs[i].icon = Ti.App.tabGroup.tabs[i].unselectedIcon;
            }
        }
    });

	Ti.App.tabGroup.addTab(tabDashboard);
	Ti.App.tabGroup.addTab(tabList);
	Ti.App.tabGroup.addTab(tabMyProfile);
	Ti.App.tabGroup.addTab(tabSettings);

	return Ti.App.tabGroup;

}
module.exports = tabgroup;
