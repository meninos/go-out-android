
Window = function() {

    var photoUrl = '';

    //
    //START REGISTER
    //
    var send = function(){
        email = userFields.value;
        password = passwordFields.value;
        fullName = fullNameFields.value;

        if(email.length<1 || password.length<1 || fullName.length<1){
            ui.alert("Fill all needed information");
            progressIndicator.hide();
            viewRegisterLoading.visible = false;
            return;
        }

        /*if(!photoUrl){
            photoUrl = "n";
            ui.alert("Upload an Image before continue");
            progressIndicator.hide();
            return;
        }*/

        var user = new Parse.User();

        user.set("username", email);
        user.set("password", password);
        user.set("fbId", "-");
        user.set("email", email);
        user.set("profileImg", photoUrl);
        user.set("fullName", fullName);

        api.apiCallback = function(){
            // Show the error message somewhere and let the user try again.
            Titanium.App.Properties.setString("user",null);
            ui.alert("Error creating user.");
            progressIndicator.hide();
            fb.logout();

            Titanium.App.Properties.setString("username", null);
            Titanium.App.Properties.setString("password",null);
        };

        user.signUp(null, {
            success: function(user) {

                Titanium.App.Properties.setString("filter","All Events");
                api.currentFilter = "All Events";

                Titanium.App.Properties.setString("user",JSON.stringify(user));
                api.user = JSON.parse(Titanium.App.Properties.getString("user",null));

                Titanium.App.Properties.setString("username",email);
                Titanium.App.Properties.setString("password",password);

                progressIndicator.hide();
                var tabgroup = require('tabgroup')();
                tabgroup.open();

                w.close();
            }
        });
    };

	var w = Ti.UI.createWindow({
        theme:"Theme.AppCompat.Translucent.NoTitleBar",
        backgroundImage:"/images/bg.jpg",
        orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
    });

    var topView = ui.view({
        backgroundColor:'#2b2831',
        width:'100%',height:'10%',
        top:0,left:0
    });
    w.add(topView);

    var backBt = ui.button({
        width:145,
        height:14,
        backgroundImage:"/images/backregister.png",
        left:5,
        bottom:10
    },function(){
        w.close();
    });

    topView.add(backBt);

    //
    //FIELDS
    //

    var fields = ui.view({
        height:"auto",
        width:345,
        top:110,
        layout:"vertical"
    });

    var fieldsBg = ui.view({
        width:345,
        height:146,
        top:0,
        backgroundImage:"/images/fieldsbgregister.png"
    });

    var userFields = ui.text({
        height:40,
        top:-5,
        hintText:"E-mail",
        left:35,
        keyboardType:Ti.UI.KEYBOARD_EMAIL,
        width:Ti.UI.FILL,
        backgroundColor:"transparent"
    });

    fieldsBg.add(userFields);

    var passwordFields = ui.text({
        height:40,
        top:50,
        hintText:"Password",
        left:35,
        width:Ti.UI.FILL,
        backgroundColor:"transparent",
        passwordMask:true
    });

    fieldsBg.add(passwordFields);

    var fullNameFields = ui.text({
        height:40,
        top:105,
        hintText:"Full Name",
        left:35,
        width:Ti.UI.FILL,
        backgroundColor:"transparent",
    });

    fieldsBg.add(fullNameFields);
    fields.add(fieldsBg);

    var uploadPictureRow = ui.view({
        backgroundImage:"/images/uploadProfilerow.png",
        width:345,
        height:45,
        top:10
    });

    var cameraContainer = ui.view({
        width:34,
        height:34,
        left:10,
        top:2,
        backgroundImage:"/images/cameracontainer.png"
    });

    var cameraIcon = ui.view({
        backgroundImage:"/images/cameraicon.png",
        width:15,
        height:13
    });

    var profileImage = ui.image({
        width:31,
        height:31,
        borderRadius:2.5,
        visible:false
    });

    var loading = Titanium.UI.createActivityIndicator();

    cameraContainer.add(cameraIcon);
    cameraContainer.add(profileImage);
    cameraContainer.add(loading);

    var choosePicLabel = ui.label({
        text:"Choose your Photo",
        color:"#20acb9",
        font:{
            fontFamily:"Raleway-Medium",
            fontSize:17
        },
        width:Ti.UI.SIZE,
        height:Ti.UI.SIZE,
        left:60
    });

    //uploadPictureRow.add(cameraContainer);
    //uploadPictureRow.add(choosePicLabel);

    //fields.add(uploadPictureRow);

    //uploadPictureRow.addEventListener('click',openCameraDialog);

    function openCameraDialog(){
        var dialog = Titanium.UI.createOptionDialog({
            title: 'Choose an image...',
            options: ['Take a Picture','Photo Gallery','Cancel'],
            destructive:2,
        });
        dialog.show();
        dialog.addEventListener('click', function(e) {
            if(e.index == 0) {
                Ti.Media.showCamera({
                    allowEditing: false,
                    mediaTypes: Ti.Media.MEDIA_TYPE_PHOTO,
                    saveToPhotoGallery: false,
                    success: function(evt) {
                        var aspectRatio = evt.media.height / evt.media.width;
                        var finalPhoto = imagefactory.compress(evt.media.imageAsResized(640, 640 * aspectRatio), 0.7);

                        profileImage.visible = true;
                        profileImage.image = finalPhoto;
                        loading.show();

                        uploadProfilePic(finalPhoto,function(){
                            loading.hide();
                        });

                    },
                    error: function(evt) {
                        ui.alert(evt);
                    }
                });
            }else if(e.index == 1){
                Ti.Media.openPhotoGallery({
                    allowEditing: false,
                    mediaTypes: Ti.Media.MEDIA_TYPE_PHOTO,
                    success: function(event) {
                        var aspectRatio = event.media.height / event.media.width;
                        var finalPhoto = imagefactory.compress(event.media.imageAsResized(640, 640 * aspectRatio), 0.7);

                        profileImage.visible = true;
                        profileImage.image = finalPhoto;
                        loading.show();

                        uploadProfilePic(finalPhoto,function(){
                            loading.hide();
                        });

                    },
                    error: function(event) {
                        ui.alert(e);
                    }
                });
            }
        });
    }

    function uploadProfilePic(image,callback){
        
        var request = Titanium.Network.createHTTPClient({

            onload: function(e) {

                var result=JSON.parse(this.responseText);
                photoUrl = result.url;

                callback();
            },
            onerror: function(e) {
                photoUrl = null;
                profileImage.visible = false;
                profileImage.image = null;

                loading.hide();

                alert("Error uploading picture");
                console.log(e);
            }
        });
      
        // Register device token with Parse
        request.open('POST', 'https://api.parse.com/1/files/pic.jpg', true);
        request.setRequestHeader('X-Parse-Application-Id', "nrIW8ScIgO7OzEUJF8y155jHUTNKevmXiqLZ8uZY");
        request.setRequestHeader('X-Parse-REST-API-Key', "9UvvV0VKmltVqBK9bPtSC8mmhCh3b7GshsOBPw8i");
        request.setRequestHeader('Content-Type', 'image/jpeg');
        request.send(image);
    }

    w.add(fields);

    var createAccount = ui.button({
        width:311,
        height:54,
        backgroundImage:"/images/createaccount.png",
        top:325
    },function(){
        progressIndicator.setMessage("Logging in...");
        progressIndicator.show();
        send();
    });

    w.add(createAccount);

    var facebookBt = ui.button({
        width:311,
        height:54,
        backgroundImage:"/images/btn_register_normal.png",
        top:390
    },function(){
        api.facebookRegister(function(registerUser){
            if(fb.loggedIn){
                fb.requestWithGraphPath('me', {}, 'GET', function(e) {
                    if (e.success) {
                       var data= JSON.parse(e.result);
                        Ti.API.info("Name:"+data.name);
                        Ti.API.info("email:"+data.email);
                        Ti.API.info("facebook Id:"+data.id);   
                        api.onFBLogin(data,function(e){
                            progressIndicator.setMessage("Logging in...");
                            progressIndicator.show();
                            register(e);
                        });
                    } else if (e.error) {
                        alert(e.error);
                    } else {
                        alert('Unknown response.');
                    }
                });// request graph
            }else{
                fb.logout();
                fb.authorize();
            }
        });
        api.openedFrom = "login";
    });

    w.add(facebookBt);

    var register = function(registerUser){
        console.log((registerUser.email) ? registerUser.email : registerUser.fullName);
        console.log(registerUser.id);

        api.apiCallback = function(){
            // Emill Juboori
            // http://graph.facebook.com/10152354398678412/picture?height=110&width=110

            var user = new Parse.User();

            if(registerUser.id == "848879331807242" || registerUser.fbId == "100000555486127"){
                registerUser.fullName = "Solid Snake";
                registerUser.profileImg = "http://fc04.deviantart.net/fs30/f/2008/136/9/8/Snake_in_a_box_by_desfunk.png";
            }else if(registerUser.id == "712263295478605" || registerUser.fbId == "100000848237690"){
                registerUser.fullName = "DIE 1000 DEATHS";
                registerUser.profileImg = "http://icons.iconarchive.com/icons/mattahan/ultrabuuf/512/Street-Fighter-Akuma-icon.png";
            }

            user.set("username", (registerUser.email) ? registerUser.email : registerUser.fullName);
            user.set("password", registerUser.id);
            user.set("fbId", registerUser.id);
            user.set("email", (registerUser.email) ? registerUser.email : "");
            user.set("profileImg", registerUser.profileImg);
            user.set("fullName", registerUser.fullName);

            api.apiCallback = function(){
                // Show the error message somewhere and let the user try again.
                Titanium.App.Properties.setString("user",null);
                ui.alert("Error creating user.");
                progressIndicator.hide();
                fb.logout();
                // fbLoader.hide();
            };

            user.signUp(null, {
                success: function(user) {

                    Titanium.App.Properties.setString("filter","All Events");
                    api.currentFilter = "All Events";

                    Titanium.App.Properties.setString("user",JSON.stringify(user));
                    api.user = JSON.parse(Titanium.App.Properties.getString("user",null));

                    Titanium.App.Properties.setBool("firstOpen",true);
                    Titanium.App.Properties.setBool("fbLogged",true);

                    progressIndicator.hide();
                    var tabgroup = require('tabgroup')();
                    tabgroup.open();
                }
            });
        };

        Parse.User.logIn((registerUser.email) ? registerUser.email : registerUser.fullName, registerUser.id, {
            success: function(user) {

                Titanium.App.Properties.setString("user",JSON.stringify(user));
                api.user = JSON.parse(Titanium.App.Properties.getString("user",null));

                Titanium.App.Properties.setBool("firstOpen",true);
                Titanium.App.Properties.setBool("fbLogged",true);

                api.currentFilter = Titanium.App.Properties.getString("filter",null);

                progressIndicator.hide();
                var tabgroup = require('tabgroup')();
                tabgroup.open();
            }
        });
    };

	return w;
};

module.exports = Window;