
var api = require("/lib/api");

var TiParse = require("/lib/TiParse")({
  	applicationId : 'nrIW8ScIgO7OzEUJF8y155jHUTNKevmXiqLZ8uZY',
  	javascriptkey : 'guL7CZb6fBrPU8rpiJWAP0IvTerS5pqeyDy9ouck'
});

var countly = require('ly.count');
if(Titanium.Network.online){
    countly.countInit('https://cloud.count.ly','d6bb7da0930462506cf2f357285f8886e803a9e2');
}

var iosVersioniosVersion = parseInt(Ti.Platform.version.split(".")[0]);

var	os = Ti.Platform.osname,
    version = Ti.Platform.version,
    height = Ti.Platform.displayCaps.platformHeight,
    width = Ti.Platform.displayCaps.platformWidth,
    density = Ti.Platform.displayCaps.density,
    ldf = (os=="android") ? Ti.Platform.displayCaps.logicalDensityFactor : 1,
    locale = Ti.Platform.locale,
    Map = require('ti.map'),
	ui = require('/lib/ui'),
	fb = require("facebook"),
	imagefactory = require("ti.imagefactory");

console.log(density);

var progressIndicator = Ti.UI.Android.createProgressIndicator({
	message: 'Loading...',
  	location: Ti.UI.Android.PROGRESS_INDICATOR_DIALOG,
  	type: Ti.UI.Android.PROGRESS_INDICATOR_INDETERMINANT,
  	cancelable: true,
});
progressIndicator.hide();

//api.subscribeDeviceToPush();;

fb.appid = "279465285567003";
fb.permissions = ["email"];

Titanium.App.Properties.setString("filter","All Events");

if(Titanium.App.Properties.hasProperty("timeFilter") && Titanium.App.Properties.hasProperty("days")){
	var today = new Date();
	var otherDay = new Date();
	otherDay.setDate(today.getDate()+Titanium.App.Properties.getInt("days",null));
	Titanium.App.Properties.setString("timeFilter",otherDay.getTime());
}else{
	var today = new Date();
	Titanium.App.Properties.setString("timeFilter",""+today.getTime());
	Titanium.App.Properties.setInt("days",0);
}

//IF FIRST TIME, OPEN TUTORIAL - ELSE GO TO DASHBOARD

if(Titanium.App.Properties.hasProperty("firstOpen")){
	if(Titanium.App.Properties.getBool("fbLogged")){
		api.user = JSON.parse(Titanium.App.Properties.getString("user"));
	}else{
		var u = JSON.parse(Titanium.App.Properties.getString("user"));
		if(u){
			api.user = u;	
		}else{
			api.user = null;	
		}
	}
    api.openedFrom = "auto";

    api.currentFilter = "All Events";

	var tabgroup = require('tabgroup')();
	tabgroup.open();
}else{
	api.user = null;
	Titanium.App.Properties.setBool("fbLogged",false);
    Titanium.App.Properties.setInt("distance",300);
    Titanium.App.Properties.setInt("days",14);

	Titanium.App.Properties.setString("filter","All Events");
	api.currentFilter = Titanium.App.Properties.getString("filter",null);
	
	var login = require('start')();
	login.open();
}