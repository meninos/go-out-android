function addEvent(){
	var w = Ti.UI.createWindow({
        theme:"Theme.AppCompat.Translucent.NoTitleBar",
        backgroundImage:"/images/bg.jpg",
        orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
        softKeyboardOnFocus:Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS,
    });

    var eventImage = "n";
    var blobToSend;

	var topView = ui.view({
        backgroundColor:'#2b2831',
        width:'100%',height:'10%',
        top:0,left:0
    });
    w.add(topView);

    var createEventLogo = Titanium.UI.createView({
        backgroundImage:'/images/title.png',
        width:123,height:14,left:'2%',
        backgroundColor:'transparent'
    });
    topView.add(createEventLogo);
    createEventLogo.addEventListener('click',function(e){w.close();});

    var scrollAddEvent = Titanium.UI.createScrollView({
        top:"10%",width:'100%',
        height:Ti.UI.FILL,
    });
    w.add(scrollAddEvent);

    var confirm = Titanium.UI.createButton({
        backgroundImage:"/images/global_accept_normal.png",
        width:40,height:32,right:'5%'
    });
    topView.add(confirm);

    confirm.addEventListener('click',function(e){
        progressIndicator.setMessage("Creating event...");
        progressIndicator.show();
        
        if(blobToSend){
            progressIndicator.setMessage("Uploading image...");
            api.uploadEventImage(blobToSend,function(e){
                eventImage = e;
                createEvent();
            });
        }else{
            createEvent();
        }
    });

    function createEvent(){

        progressIndicator.setMessage("Creating event...");
        finalHour = (hourText.getValue()[0] == "0") ? parseInt(hourText.getValue()[1]) : parseInt(hourText.getValue());
        finalMinutes = (minuteText.getValue()[0] == "0") ? parseInt(minuteText.getValue()[1]) : parseInt(minuteText.getValue());

        if(btAmPm.ampm == 'pm' && finalHour!=12){
            finalHour = finalHour + 12;
        }

        finalDate.setHours(finalHour,finalMinutes);

        if(!eventName.value || eventName.value == "Enter Title") { 
            send(null,"name");
            return;
        }
        if(!location) {
            send(null,"location");
            return;
        };

        var payload = {
            filter:category,
            name:eventName.value,
            description:(eventDescription.value == "Enter Description") ? "" : eventDescription.value,
            date:finalDate,
            repeats:repeats,
            location:location,
            address: fullAddress,
            image:(eventImage) ? eventImage : "n"
        };
        
        send(payload,false);
    }

    var Map = require('ti.map');
    //
    //MAP VIEW
    //
    var map = Map.createView({
    //var map = Ti.UI.createView({
        width:"100%",top:0,height:165,
        userLocation:true,
    });
    scrollAddEvent.add(map);

    var repeats = "NO";
    var category="Happy Hours";
    var eventPin = null;

    var finalDate = new Date();
    var finalHour = finalDate.getHours();
    var finalMinutes = finalDate.getMinutes();

    //EVENT NAME TEXT FIELD
    var eventName = Titanium.UI.createTextField({
        hintText:"Enter Title",
        top:170,width:"93%",color:'#959596',
        font:{fontFamily:"Raleway-Regular",fontSize:16},
    });
    scrollAddEvent.add(eventName);

    eventName.addEventListener('change',function(e){
        if(eventPin){
            eventPin.setTitle(eventName.value);
        }
    });

    var locationIcon = Titanium.UI.createView({
        top:220,left:'4.5%',width:13,height:16,
        backgroundImage:"/images/ic_7_location.png",
    });
    scrollAddEvent.add(locationIcon);

    var eventLocation = Titanium.UI.createTextField({
        hintText:"Enter Location", 
        top:210,width:"82%",left:"12%",
        font:{fontFamily:"Raleway-Regular",fontSize:16},
        color:'#959596',
        returnKeyType:Titanium.UI.RETURNKEY_SEARCH,
        hint:"Enter Location"
    });
    scrollAddEvent.add(eventLocation);

    var loadingLocation = Ti.UI.createActivityIndicator({
        color: 'white',top:221,right:3,
        style:Ti.UI.ActivityIndicatorStyle.PLAIN,
    });
    scrollAddEvent.add(loadingLocation);

    var duplicate = false;
    eventLocation.addEventListener("focus",function(e){
        if(this.value == this.hint){
            this.value = "";
        }
        duplicate = false;
    });
    eventLocation.addEventListener("blur",function(e){
        if(!duplicate){
            duplicate = true;
            if(this.value.length < 1){
                this.value = "Enter Location";
            }else{
                map.removeAllAnnotations();
                loadingLocation.show();
                api.searchAPI(this.value,callbackSearch);
            }
        }
    });

    eventLocation.addEventListener("return",function(e){
        if(!duplicate){
            duplicate = true;
            if(this.value.length<1){
                this.value = "Enter Location";
            }else{
                map.removeAllAnnotations();
                loadingLocation.show();
                api.searchAPI(this.value,callbackSearch);
            }
        }
    });

    var dateView = Titanium.UI.createView({
        backgroundImage:"/images/ic_7_time.png",
        top:270,left:"4.5%",
        width:16,height:16
    });
	scrollAddEvent.add(dateView);

	var dateText = Ti.UI.createLabel({
		top:270,left:'12%',
		color:'white',text:'Date',
		font:{fontFamily:"Raleway-Medium",fontSize:14},
	});
	scrollAddEvent.add(dateText);

	var containerDate = Titanium.UI.createView({
        layout:"horizontal",top:260,
        right:"2%",width:Ti.UI.SIZE,height:37,
    });
    scrollAddEvent.add(containerDate);

    var hourText = Titanium.UI.createTextField({
        value:ui.formatAMPM(finalDate).hour,color:'#20acb9',
        font:{fontFamily:"Raleway-SemiBold",fontSize:18},
        right:"-1%",maxLength:2,
        keyboardType:Titanium.UI.KEYBOARD_NUMBER_PAD,
        returnKeyType:Ti.UI.RETURNKEY_DONE,
        width:Ti.UI.SIZE
    });
    containerDate.add(hourText);

    var labelSeparator = Titanium.UI.createLabel({
        text:":",color:'#20acb9',right:"0%",
        font:{fontFamily:"Raleway-Regular",fontSize:14},
    });
    containerDate.add(labelSeparator);

    var minuteText = Titanium.UI.createTextField({
        value:(finalDate.getMinutes() < 10) ? "0"+finalDate.getMinutes() : finalDate.getMinutes(), 
        font:{fontFamily:"Raleway-SemiBold",fontSize:18},
        color:'#20acb9',right:"2%",maxLength:2,
        keyboardType:Titanium.UI.KEYBOARD_NUMBER_PAD,
        returnKeyType : Ti.UI.RETURNKEY_DONE,
        width:Ti.UI.SIZE,
    });
    containerDate.add(minuteText);

    var btAmPm = Titanium.UI.createButton({
        backgroundImage: (ui.formatAMPM(finalDate).ampm == "PM") ? "/images/ic_7_pm.png" : "/images/ic_7_am.png",
        ampm:(ui.formatAMPM(finalDate).ampm == "PM") ? "pm" : "am",
        width:54,height:35,left:3,
    });
    containerDate.add(btAmPm);
    btAmPm.addEventListener("click",function(e){
        if(this.ampm == "pm"){
            this.backgroundImage = "/images/ic_7_am.png";
            this.ampm = "am";
        }else{
            this.backgroundImage = "/images/ic_7_pm.png";
            this.ampm = "pm";
        }
    });

    var dateNumber = Titanium.UI.createLabel({
        text:"Today",color:'#20acb9',
        font:{fontFamily:"Raleway-SemiBold",fontSize:18},
        width:Ti.UI.SIZE,left:5,height:20,
    });
    containerDate.add(dateNumber);

    //
    //SLIDER DAYS SETUP
    //
    var sliderDays = Titanium.UI.createSlider({
        width:'90%',top:305,
        thumbImage:"/images/sliderball.png",
        tintColor:"#20acb9",max:60,
        min:0,value:0
    });
    scrollAddEvent.add(sliderDays);

    sliderDays.addEventListener("change",function(e){
        e.value = Math.round(e.value);
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        today = new Date();
        var otherDay = new Date();
        otherDay.setDate(today.getDate()+e.value);
        var day = (otherDay.getDate()<10) ? "0"+otherDay.getDate() : otherDay.getDate();
        var month = months[otherDay.getMonth()];
        var year = otherDay.getFullYear();
        if(e.value < 1){
            dateNumber.text = "Today";
        }else{
            dateNumber.text = month+" "+day;
        }
        finalDate = new Date(otherDay);
    });

    //
    //REPEAT BUTTONS SETUP
    var repeatContainer = Titanium.UI.createView({
    	layout:"horizontal",top:340,
    	width:'100%',height:40,
    });
    scrollAddEvent.add(repeatContainer);

    var repeatIconView = Titanium.UI.createView({
        left:"4.5%",
        width:80,height:16,
        backgroundImage:"/images/repeatText.png",
    });
    repeatContainer.add(repeatIconView);

    var btNo = Titanium.UI.createButton({
        left:"7%",width:54,height:35,
        sImage:'/images/btNoS.png',
        oImage:'/images/btNo.png',
        id:0,backgroundImage:"/images/btNoS.png",
        value:"NO",
    });
    repeatContainer.add(btNo);
    btNo.addEventListener("click",function(e){
        repeatHandler(this.id);
    });

    var btDaily = Titanium.UI.createButton({
        left:"3%",id:1,
        backgroundImage:'/images/btDaily.png',
        sImage:'/images/btDailyS.png',
        oImage:'/images/btDaily.png',
        width:68,height:35,
        value:"DAILY",
    });
    repeatContainer.add(btDaily);
    btDaily.addEventListener("click",function(e){
        repeatHandler(this.id);
    });

    var btWeekly = Titanium.UI.createButton({
        left:"3%",id:2,
        backgroundImage:'/images/btWeekly.png',
        sImage:'/images/btWeeklyS.png',
        oImage:'/images/btWeekly.png',
        width:89,height:35,
        value:"WEEKLY",
    });
    repeatContainer.add(btWeekly);
    btWeekly.addEventListener("click",function(e){
        repeatHandler(this.id);
    });
    var repeatBts = [btNo,btDaily,btWeekly];

    function repeatHandler(id){
    	repeatBts.forEach(function(repeatBts){
	        if(id!=repeatBts.id){
	            repeatBts.backgroundImage = repeatBts.oImage;
	        }else{
	            repeats = repeatBts.value;
	            repeatBts.backgroundImage = repeatBts.sImage;
	        }
	    });
	};

	//
	//CATEGORIES SCROLL
	var btsCategories = [];
	var scrollCategories = Titanium.UI.createScrollView({
        top:380,height:38,width:"100%",
        layout:"horizontal",scrollType:'horizontal',
    });
    scrollAddEvent.add(scrollCategories);

    populateCategories(scrollCategories);

    function populateCategories(scroll){
	    var iconHappyHour = Titanium.UI.createImageView({
	        image:"/images/ic_7_happy_inactive.png",
	        pressed:true,
	        category:"Happy Hours",
	        selected:"/images/ic_7_happy_active.png",
	        unselected:"/images/ic_7_happy_inactive.png",
	        id:0,
	        left:16
	    });
	    btsCategories.push(iconHappyHour);

	    var iconSpecials = Titanium.UI.createImageView({
	        image:"/images/ic_7_specials_inactive.png",
	        pressed:false,
	        category:"Specials",
	        selected:"/images/ic_7_specials_active.png",
	        unselected:"/images/ic_7_specials_inactive.png",
	        id:1,
	        left:16
	    });
	    btsCategories.push(iconSpecials);

	    var iconLive = Titanium.UI.createImageView({
	        image:"/images/ic_7_live_inactive.png",
	        pressed:false,
	        category:"Live Music",
	        selected:"/images/ic_7_live_active.png",
	        unselected:"/images/ic_7_live_inactive.png",
	        id:2,
	        left:16
	    });
	    btsCategories.push(iconLive);

	    var iconEDM = Titanium.UI.createImageView({
	        image:"/images/ic_7_edm_inactive.png",
	        pressed:false,
	        category:"EDM",
	        selected:"/images/ic_7_edm_active.png",
	        unselected:"/images/ic_7_edm_inactive.png",
	        id:3,
	        left:16
	    });
	    btsCategories.push(iconEDM);

	    var iconConcerts = Titanium.UI.createImageView({
	        image:"/images/ic_7_concerts_inactive.png",
	        pressed:false,
	        category:"Concerts",
	        selected:"/images/ic_7_concerts_active.png",
	        unselected:"/images/ic_7_concerts_inactive.png",
	        id:4,
	        left:16
	    });
	    btsCategories.push(iconConcerts);

	    var iconFoods = Titanium.UI.createImageView({
	        image:"/images/ic_7_food_inactive.png",
	        pressed:false,
	        category:"Food Specials",
	        selected:"/images/ic_7_food_active.png",
	        unselected:"/images/ic_7_food_inactive.png",
	        id:5,
	        left:16
	    });
	    btsCategories.push(iconFoods);

	    var iconSports = Titanium.UI.createImageView({
	        image:"/images/ic_7_sporting_inactive.png",
	        pressed:false,
	        category:"Sporting Events",
	        selected:"/images/ic_7_sporting_active.png",
	        unselected:"/images/ic_7_sporting_inactive.png",
	        id:6,
	        left:16
	    });
	    btsCategories.push(iconSports);

	    var iconCars = Titanium.UI.createImageView({
	        image:"/images/ic_7_car_inactive.png",
	        pressed:false,
	        category:"Car Events",
	        selected:"/images/ic_7_car_active.png",
	        unselected:"/images/ic_7_car_inactive.png",
	        id:7,
	        left:16
	    });
	    btsCategories.push(iconCars);

	    var iconKaraoke = Titanium.UI.createImageView({
	        image:"/images/ic_7_karaoke_inactive.png",
	        pressed:false,
	        category:"Karaoke",
	        selected:"/images/ic_7_karaoke_active.png",
	        unselected:"/images/ic_7_karaoke_inactive.png",
	        id:8,
	        left:16
	    });
	    btsCategories.push(iconKaraoke);

	    var iconFarmers = Titanium.UI.createImageView({
	        image:"/images/ic_7_farmers_inactive.png",
	        pressed:false,
	        category:"Farmers Market",
	        selected:"/images/ic_7_farmers_active.png",
	        unselected:"/images/ic_7_farmers_inactive.png",
	        id:9,
	        left:16
	    });
	    btsCategories.push(iconFarmers);

	    var iconOthers = Titanium.UI.createImageView({
	        image:"/images/ic_7_other_inactive.png",
	        pressed:false,
	        category:"Others",
	        selected:"/images/ic_7_other_active.png",
	        unselected:"/images/ic_7_other_inactive.png",
	        id:10,
	        left:12
	    });
	    btsCategories.push(iconOthers);

	    btsCategories.forEach(function(e){
	    	scroll.add(e);
	        e.addEventListener("click",function(e){
	            if(eventPin){
	                eventPin.setImage("/images/"+ui.filters[this.category]);
                    map.removeAllAnnotations();
                    map.addAnnotation(eventPin);
                    map.selectAnnotation(eventPin);
	            }

	            this.pressed = true;
	            this.setImage(this.selected);
	            category = this.category;

	            for(var i = 0; i < btsCategories.length; i++){
	                if(this.id != i){
	                    btsCategories[i].pressed=false;
	                    var image = (btsCategories[i].pressed) ? btsCategories[i].selected : btsCategories[i].unselected;
	                    btsCategories[i].setImage(image);
	                }
	            }
	        });
	    });

	    var marginRight = Titanium.UI.createView({
	        width:30,left:6
	    });
	    scroll.add(marginRight);
	};

	//
	//EVENT DESCRIPTION

	var descriptionAndAddImage = Titanium.UI.createView({
        top:420,height:200,width:"93%",layout:"vertical"
    });
	scrollAddEvent.add(descriptionAndAddImage);

	var eventDescription = Titanium.UI.createTextArea({
        value:"Enter Description",top:0,
        font:{fontFamily:"Raleway-Regular",fontSize:16},
        color:'#959596',width:"93%",height:150,
        backgroundColor:"transparent",
        hint:"Enter Description",suppressReturn:false
    });
    descriptionAndAddImage.add(eventDescription);
    eventDescription.addEventListener("focus",function(e){
        if(this.value==this.hint){
            this.value = "";
        }
    });
    eventDescription.addEventListener("blur",function(e){
        if(this.value.length<1){
            this.value = "Enter Description";
        };
    });

    var imgContainer = Titanium.UI.createImageView({
        width:300,height:0,top:0,
    });
    descriptionAndAddImage.add(imgContainer);
    //imgContainer.add(loadingView);

    var addImageContainer = Ti.UI.createView({
    	top:17,left:10,width:200,height:20,
    });
	descriptionAndAddImage.add(addImageContainer);
    
    var btAddImg = Titanium.UI.createButton({
        backgroundImage:"/images/ic_7_addimage_normal.png",
        left:0,width:29,height:19,
    });
    addImageContainer.add(btAddImg);

    var addImageText = Ti.UI.createLabel({
		bottom:0,left:33,color:'#20acb9',text:'Add Image',
		font:{fontFamily:"Raleway-Medium",fontSize:14},
	});
	addImageContainer.add(addImageText);

	addImageContainer.addEventListener('click',openCameraDialog);
	function openCameraDialog(){
		var dialog = Titanium.UI.createOptionDialog({
            title: 'Choose an image...',
            options: ['Take a Picture','Photo Gallery','Cancel'],
            destructive:2,
        });
        dialog.show();
        dialog.addEventListener('click', function(e) {
            if(e.index == 0) {
            	Ti.Media.showCamera({
                	allowEditing: true,
                    showControls:true,
	                mediaTypes: Ti.Media.MEDIA_TYPE_PHOTO,
	                saveToPhotoGallery: false,
	                success: function(evt) {
	                    var image = evt.media;
                        image = evt.media.imageAsThumbnail(194, 0, 0);
                        var aspectRatio = evt.media.height / evt.media.width;
                        blobToSend = imagefactory.compress(image.imageAsResized(640, 640 * aspectRatio), 0.7);

                        insertImage(blobToSend);

                        /*api.uploadEventImage(blobToSend,function(e){
                            eventImage = e;
                            loadingView.visible = false;
                        });*/
	                },
	                error: function(evt) {
	                    ui.alert(evt);
	                }
	            });
            }else if(e.index == 1){
                Ti.Media.openPhotoGallery({
	                allowEditing: true,
	                mediaTypes: Ti.Media.MEDIA_TYPE_PHOTO,
	                success: function(evt) {
	                    var image = evt.media;
                        image = evt.media.imageAsThumbnail(194, 0, 0);
                        var aspectRatio = evt.media.height / evt.media.width;
                        blobToSend = imagefactory.compress(image.imageAsResized(640, 640 * aspectRatio), 0.7);

                        insertImage(blobToSend);

                        /*api.uploadEventImage(blobToSend,function(e){
                            eventImage = e;
                            loadingView.visible = false;
                        });*/
	                },
	                error: function(event) {
	                    ui.alert(e);
	                }
	            });
            }
        });
	}

	function insertImage(blob){
        //closeBt.visible = true;
        //loadingView.visible = true;
        imgContainer.height = 250;
        descriptionAndAddImage.height = 500;
        imgContainer.setImage(blob);
    };

    function callbackSearch(result,address,error){
	    if(error){
	        location = null;
	        loadingLocation.hide();
	        return;
	    }

	    loadingLocation.hide();
	    ui.setLocation(map,result.results[0].geometry.location,"addevents");

	    var coords = result.results[0].geometry.location;
	    fullAddress = address;
	    eventLocation.value = address;
	    location = coords;

	    eventPin = Map.createAnnotation({
	        latitude: location.lat,
	        longitude: location.lng,
	        title: eventName.value,
	        draggable:false,
	        centerOffset: {x: -0, y: -25},
	        image:"/images/"+ui.filters[category]
	    });

	    map.addAnnotation(eventPin);
	    map.selectAnnotation(eventPin);

	    map.addEventListener("pinchangedragstate",function(r){
	        if(r.newState == 0){
	            location = {lat:r.annotation.latitude,lng:r.annotation.longitude};
	        };
	    });
	};

    function send(payload,error){
        if(error){
            progressIndicator.hide();
            var errors = {
                "name":"Enter a name for the Event",
                "description":"Enter a description for the Event",
                "location":"Pick a location for the Event",
                "date":"Event date must be higher then current time"
            };
            alert(errors[error]);
        }else{
            api.createNewEvent(payload,function(){
                ui.setLocation(map,{lat:api.currentLat,lng:api.currentLng});
                if(error){
                    alert("There was a problem creating the Event.\nTry again later");
                    progressIndicator.hide();
                    return;
                }else{
                    setTimeout(function(e){
                        progressIndicator.hide();
                        Ti.App.edited = true;
                        api.newDate = true;
                        w.close();
                        setTimeout(function(e){
                            var toast = Ti.UI.createNotification({
                                message:"Event successfully created",
                                duration: Ti.UI.NOTIFICATION_DURATION_LONG
                            });
                            toast.show();
                        },200);
                    },200);
                }
            });
        }
    };
    

	return w;
}

module.exports = addEvent;