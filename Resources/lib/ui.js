


exports.myProfileBlockView = null;
exports.addEventBlockView = null;


exports.view = function(args) {
    return Ti.UI.createView(args);
};

exports.image = function(args){
    return Ti.UI.createImageView(args);
};

exports.video = function(args){
    var movie = Ti.Media.createVideoPlayer(args);

    movie.addEventListener('complete', function (e) {
        //alert("movie is finished");
        movie.play();  
    });

    return movie;
};

exports.animation = function(args) {
    return Ti.UI.createAnimation(args);
};

exports.list = function(args) {
    return Ti.UI.createListView(args);
};

exports.listsection = function(args) {
    return Ti.UI.createListSection(args);
};

exports.text = function(args){
    return Ti.UI.createTextField(args);
};

exports.label = function(args){
    return Ti.UI.createLabel(args);
};

exports.loading = function(args){
    return Ti.UI.createActivityIndicator(args);
};  

exports.table = function(args) {
    return Ti.UI.createTableView(args);
};

exports.row = function(args) {

    if(args){
        if(!args["color"]){
            args["color"] = 'black';
        }
    }

    return Ti.UI.createTableViewRow(args);
};

exports.section = function(args) {
    return Ti.UI.createTableViewSection(args);
};

exports.searchBar = function(args) {
    return Ti.UI.createSearchBar(args);
};

exports.tabGroup = function(args) {
    return Ti.UI.createTabGroup(args);
};

exports.tab = function(args) {
    return Ti.UI.createTab(args);
};

exports.button = function(args,fn){
    var bt = Titanium.UI.createButton(args);

    bt.addEventListener("click",fn);

    return bt;
};


exports.normalizeString = function(s){
    var r=s.toLowerCase();
    r = r.replace(new RegExp("[àáâãäå]", 'g'),"a");
    r = r.replace(new RegExp("æ", 'g'),"ae");
    r = r.replace(new RegExp("ç", 'g'),"c");
    r = r.replace(new RegExp("[èéêë]", 'g'),"e");
    r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
    r = r.replace(new RegExp("ñ", 'g'),"n");                            
    r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
    r = r.replace(new RegExp("œ", 'g'),"oe");
    r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
    r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
    return r;  
};

exports.formatAMPM = function(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    if(hours < 10){
        hours = '0' + hours;
    }
    if(minutes < 10){
        minutes = '0' + minutes;
    }
    var strTime = {
        hour:hours,
        minute:minutes,
        ampm:ampm
    };
    return strTime;
};

exports.calculateDistance = function(lat1, lon1, lat2, lon2, unit) {

    var radlat1 = Math.PI * lat1/180;
    var radlat2 = Math.PI * lat2/180;
    var radlon1 = Math.PI * lon1/180;
    var radlon2 = Math.PI * lon2/180;
    var theta = lon1-lon2;
    var radtheta = Math.PI * theta/180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;
    dist = dist * 1.609344;

    if(unit){
        dist = dist/1.6;
        return parseValue(dist,1);
    }else{
        return dist;
    }
};

parseValue = function(value, value_to_round){
    value /= value_to_round;
    value = Math.round(value);
    value *= value_to_round;
    return value;
};

exports.filters = {
    "All Events":"allevents.png",
    "Concerts":"ic_1_pin_concerts.png",
    "Farmers Market":"ic_1_pin_farmers.png",
    "Happy Hours":"ic_1_pin_happy.png",
    "Karaoke":"ic_1_pin_karaoke.png",
    "Live Music":"ic_1_pin_live.png",
    "Concerts":"ic_1_pin_live.png",
    "Others":"ic_1_pin_other.png",
    "Specials":"ic_1_pin_street_specials.png",
    "Sporting Events":"ic_1_pin_sport.png",
    "Street Fairs":"ic_1_pin_street_fairs.png",
    "EDM":"ic_1_pin_edm.png",
    "Food Specials":"ic_1_pin_food.png",
    "Car Events":"ic_1_pin_car.png"
};


exports.getLocation = function(map,callback){

    Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    Titanium.Geolocation.getCurrentPosition(function(e)
    {
    if (!e.success || e.error)
        {
            return;
        }
        api.currentLng = e.coords.longitude;
        api.currentLat = e.coords.latitude;

        var loc = {lat:api.currentLat,lng:api.currentLng};
        //set Map Region to User
        if(Ti.Network.online && map) {
            map.setLocation({
                latitude : loc.lat,
                longitude : loc.lng,
                latitudeDelta : 0.15,
                longitudeDelta : 0.15
            });
        }
    });
};

exports.alert = function(args, showAlert) {
    showAlert = (showAlert == null || showAlert == true) ? true : false;
    if(args.title == null) {
        args = {
            title: 'Go Out',
            message: args
        };
    }
    var diag = Ti.UI.createAlertDialog(args);
    if (!showAlert) {
        return diag;
    } else {
        diag.show();
    }
};

exports.setLocation = function(map,loc,from){

    var offSet = (from == "addevents") ? 0.018 : 0;

    map.setLocation({
        latitude : loc.lat+offSet,
        longitude : loc.lng,
        latitudeDelta : 0.10,
        longitudeDelta : 0.10
    });
};