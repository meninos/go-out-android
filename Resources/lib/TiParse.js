/**
 * 
 * Aaron K. Saunders
 * twitter: @aaronksaunders
 * last updated may 23, 2014
 *
 * See more Appcelerator Information on Clearly Innovative Blog
 *
 * www.clearlyinnovative.com
 * blog.clearlyinnovative.com
 *   http://www.clearlyinnovative.com/blog/post/34758525607/parse-appcelerator-titanium-the-not-so-easy-way
 *
 * http://www.clearlyinnovative.com
 *
 * Copyright 2014 Clearly Innovative, Inc.
 * The Parse-Appcelerator JavaScript Hack is freely distributable under the MIT license.
 *
 */

/**
 * Edits to the parse library to work with this helper file
 *
 * Add this to line 1348
 *<pre>
 * // Import Parse's local copy of underscore.
 *    if (Titanium !== "undefined") {
 *      console.log("Using titanium");
 *
 *      Parse._ = exports._.noConflict();
 *      exports.Parse = Parse;
 *    } else
 *</pre>
 *
 * Replace line 8576
 * <pre>
 * Parse.User._registerAuthenticationProvider(FB.provider);
 * </pre>
 */

var TiParse = function(options) {
	
	debugger;

  // UPDATED TO LATEST PARSE LIBRARY version parse-1.2.18
  require("lib/parse-1.2.18");

  //
  // Override the Facebook object on Appcelerator
  //
  // Create the provider and the other methods the Parse Library is looking for
  //

  /**
   * over write the local storage so it works on Appcelerator
   */
  Parse.localStorage = {
    getItem : function(key) {
      return Ti.App.Properties.getObject(Parse.localStorage.fixKey(key));
    },
    setItem : function(key, value) {
      return Ti.App.Properties.setObject(Parse.localStorage.fixKey(key), value);
    },
    removeItem : function(key, value) {
      return Ti.App.Properties.removeProperty(Parse.localStorage.fixKey(key));
    },
    //Fix Parse Keys. Parse uses a Key containing slashes "/". This is invalid for Titanium Android
    //We'll replace those slashes with underscores ""
    fixKey : function(key) {
      return key.split("/").join("");
    }
  };

  //
  // Enter appropriate parameters for initializing Parse
  //
  // options.applicationId, options.javascriptkey);
  //
Parse.initialize(options.applicationId, options.javascriptkey);
  /**
   * Over write the _ajax function to use titanium httpclient
   *
   * @TODO Still looking for way to clean this up better
   *
   * @param {Object} method
   * @param {Object} url
   * @param {Object} data
   * @param {Object} success
   * @param {Object} error
   */
  Parse._ajax = function(method, url, data, success, error) {

    var options = {
      success : success,
      error : error
    };

    var promise = new Parse.Promise();
    var handled = !1;
    var xhr = Ti.Network.createHTTPClient({
      timeout : 5e3
    });
    xhr.onreadystatechange = function() {
      if (4 === xhr.readyState) {
        if (handled)
          return;
        handled = !0;
        if (xhr.status >= 200 && 300 > xhr.status) {
          var response;
          try {
            response = JSON.parse(xhr.responseText);
          } catch (e) {
            promise.reject(e);
          }
          response && promise.resolve(response, xhr.status, xhr);
        } else
          promise.reject(xhr);
      }
    };

    xhr.onerror = function(){
        api.apiCallback();
        api.apiCallback = null;
    }

    xhr.open(method, url, !0);
    xhr.setRequestHeader("Content-Type", "text/plain");
    xhr.send(data);
    return promise._thenRunCallbacks(options);
  };

  //
  // IF the appid was set for facebook then initialize facebook. if you are going
  // to use Facebook, set the appid at the top of this file
  //
};
module.exports = TiParse;