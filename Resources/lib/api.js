
exports.deviceModel = (Titanium.Platform.displayCaps.platformHeight>480) ? "iphone5" : "iphone4";

exports.currentFilter = "All Events";

exports.newDate = false;

exports.currentLat;

exports.isCurrentLocation = true;

exports.currentLng;

exports.loadedOnceMap = false;
exports.loadedOncelist = false;
exports.loadedOnceMy = false;

exports.apiKey = "AIzaSyADLIWwwOEFQwZ5o6vfd6MSkFitqQopLaA";

exports.user = null;

exports.openedFrom = null;

exports.filtersBar = {
    "All Events":"Ti.Codec.CHARSET_ISO_LATIN_1_all_events.png",
    "Concerts":"ic_1_concerts.png",
    "Farmers Market":"ic_1_farmers.png",
    "Happy Hours":"ic_1_happy.png",
    "Karaoke":"ic_1_karaoke.png",
    "Live Music":"ic_1_live.png",
    "Concerts":"ic_1_live.png",
    "Others":"ic_1_other.png",
    "Specials":"ic_1_specials.png",
    "Sporting Events":"ic_1_sporting.png",
    "Street Fairs":"ic_1_farmers.png",
};

exports.filtersIcon = {
    "Concerts":"icconcerts2.png",
    "Farmers Market":"ic_3_farmers.png",
    "Happy Hours":"ic_3_happy.png",
    "Karaoke":"ic_3_karaoke.png",
    "Live Music":"ic_3_live.png",
    "Concerts":"ic_3_live.png",
    "Others":"ic_3_other.png",
    "Specials":"ic_3_specials.png",
    "Sporting Events":"ic_3_sport.png",
    "Street Fairs":"ic_3_street_fairs.png",
    "EDM":"ic_3_edm.png",
    "Food Specials":"ic_3_food.png",
    "Car Events":"ic_3_car.png"
};  

exports.apiCallback = null;

// Photo Cache Dir
var acd = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'eventr');
if (!acd.exists()) {
    acd.createDirectory(); 
}

// Helper Methods
exports.getf = function(fname) {
    var f = Ti.Filesystem.getFile(acd.resolve(), fname);
    return f;
};

exports.searchAPI = function(address,callback,latLng){
    //
    //If String contains 'Av',"Avenue", "St", "Street" use Location URL call - else use Places URL call
    //
    var isPlace = true;
    address = ui.normalizeString(address);
    var valuesToCheck = [
        " ave",
        " avenue",
        " street",
        "rua",
        "avenida",
        " drive",
        " place",
        " dr",
        " road",
        " rd",
        " court",
        " ct",
        " pl",
        " st",
        " blvd",
        " boulevard",
        " alley",
        " dirt road",
        " frontage road",
        " highway",
        " route",
        " byway",
        " lane",
        " backroad"
    ];
    valuesToCheck.forEach(function(valuesToCheck) {
        if(address.indexOf(valuesToCheck) >= 0){
            isPlace = false;
        }
    });

    if(!exports.currentLat || !exports.currentLng){
        alert("Unable to get your current location. Maybe your GPS is turned off.");
        return;
    }

    var url;
    var placeApi = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+exports.currentLat+","+exports.currentLng+"&rankby=distance&name="+address+"&sensor=false&key="+exports.apiKey;
    var addressApi = "http://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=true";

    if(!isPlace || latLng){
        url = addressApi;
    }else{
        url = placeApi;
    }

    var request = Titanium.Network.createHTTPClient();
    request.timeout = 7000;
    request.open("GET", url);
    request.onload = function()
    {
        var result = JSON.parse(this.responseText);
        if(result.status == "OK"){
            if(!isPlace){
                var address = result.results[0].formatted_address;
            }else{
                var address = result.results[0].vicinity;
                console.log(address);
            }
            callback(result,address,false);
        }else{
            console.log(result);
            ui.alert("No results for your search");
            callback([],null,true);
        }
    };
    request.ontimeout= function(e){
        callback([],null,true);
        ui.alert("No results for your search");
    };
    request.onerror = function(e){
        console.log("error search   "+e);
        callback([],null,true);
        ui.alert("No results on the search.");
    };
    request.send();
};

exports.getLocationAddress = function(result,error){

    var finalString = "";

    if(!error){

        var city;
        var state;
        var country;

        for(var i = 0 ; i<result.results[0].address_components.length ; i++){
            
            var obj=result.results[0].address_components[i];
            
            obj.types.forEach(function(str){
                if(str == "locality"){
                    city = obj.long_name;
                }
                if(str == "country"){
                    country = obj.short_name;
                }
                if(str == "administrative_area_level_1"){
                    state = obj.short_name;
                }
            });
        };
        
        finalString = city+" - "+state+", "+country;

        if(api.user){
            Parse.User.logIn(api.user.email, api.user.fbId, {
                success: function(user) {
                    user.set("from",finalString);
                    user.save(null, {
                        success: function(user) {
                            Titanium.App.Properties.setString("user",JSON.stringify(user));
                            api.user = JSON.parse(Titanium.App.Properties.getString("user",null));
                        }
                    });
                }
            });
        }

        return finalString;
    }
};

exports.editEvent = function(evt,info,callback){
  evt.set("name", info.name);
  evt.set("category", info.filter); 
  evt.set("date", info.date);
  var point = new Parse.GeoPoint({latitude: info.location.lat, longitude: info.location.lng});
  evt.set("location", point);
  evt.set("description", info.description);
  evt.set("repeats", info.repeats);
  evt.set("createdby", api.user.objectId);
  evt.set("address", info.address);
  evt.set("eventimage", (info.image) ? info.image : "");

  exports.apiCallback = function(){
    callback(true);
  };

  evt.save(null, {
    success: function(post) {
        callback(false);
    }
  });

};

exports.resetPass = function(mail){

    exports.apiCallback = function(){
        // Show the error message somewhere
        ui.alert(error.message);
    };

    Parse.User.requestPasswordReset(mail, {
        success: function() {
            ui.alert("E-mail sent");
        }
    });
};

exports.uploadEventImage = function(image,callback){
    
    var request = Titanium.Network.createHTTPClient({

        onload: function(e) {

            var result=JSON.parse(this.responseText);
            var url=result.url;

            callback(url);
        },
        onerror: function(e) {
            alert("Error uploading image");
            console.log("upload error");
            console.log(e);
        }
    });
  
    // Register device token with Parse
    request.open('POST', 'https://api.parse.com/1/files/pic.jpg', true);
    request.setRequestHeader('X-Parse-Application-Id', "nrIW8ScIgO7OzEUJF8y155jHUTNKevmXiqLZ8uZY");
    request.setRequestHeader('X-Parse-REST-API-Key', "9UvvV0VKmltVqBK9bPtSC8mmhCh3b7GshsOBPw8i");
    request.setRequestHeader('Content-Type', 'image/jpeg');
    request.send(image);
};

exports.getEventAuthor = function(id,callback){

    exports.apiCallback = function(){
        callback(null,true);
    };

    var query = new Parse.Query(Parse.User);
    query.get(id, {
        success: function(user) {
            callback(user,false);
        },error: function(user, error) {
            callback(null,true);
        }
    });
};

exports.createNewEvent = function(info,callback){

    var Event = Parse.Object.extend("Event");
    var eventPost = new Event();

    eventPost.set("name", info.name);
    eventPost.set("category", info.filter); 
    eventPost.set("date", info.date);
    var point = new Parse.GeoPoint({latitude: info.location.lat, longitude: info.location.lng});
    eventPost.set("location", point);
    eventPost.set("description", info.description);
    eventPost.set("repeats", info.repeats);
    eventPost.set("createdby", api.user.objectId);
    eventPost.set("address", info.address);
    eventPost.set("eventimage", info.image);

    exports.apiCallback = function(){
        console.log("ERROR  "+post);
        callback(false);  
    };

    eventPost.save(null, {
      success: function(post) {
          callback(true);
      }
    });
};

exports.returnEventsNearby = function(callback){
    // User's location
    var userGeoPoint = new Parse.GeoPoint({latitude: exports.currentLat, longitude: exports.currentLng});

    var Event = Parse.Object.extend("Event");

    // Create a query for places
    var query = new Parse.Query(Event);

    query.ascending("date");

    var d = new Date();
    var today = new Date();
    today.setHours(00,00);
    d.setHours(23,59);
    d.setDate(d.getDate()+Titanium.App.Properties.getInt("days",null));
    var expirationDate = new Date(d.getTime()); 
    var expirationDate_utc = new Date(expirationDate.getUTCFullYear(), expirationDate.getUTCMonth(), expirationDate.getUTCDate(),  expirationDate.getUTCHours(), expirationDate.getUTCMinutes(), expirationDate.getUTCSeconds());

    query.lessThanOrEqualTo( "date", expirationDate_utc );
    query.greaterThanOrEqualTo( "date", today );
    
    if(exports.isCurrentLocation){
        query.withinMiles("location", userGeoPoint,Titanium.App.Properties.getInt("distance",null));
    }else{
        query.withinMiles("location", userGeoPoint,1000);
    }

    if(api.currentFilter != "All Events"){
        query.containedIn("category", api.currentFilter);
    }
    // Final list of objects

    exports.apiCallback = function(){
        //ui.alert("Connection failure\nTry again soon.");
        var toast = Ti.UI.createNotification({
            message:"Connection failure\n Please, check your internet.",
            duration: Ti.UI.NOTIFICATION_DURATION_LONG
        });
        toast.show();
    };

    query.find({
        success: function(placesObjects) {
            if(placesObjects.length>0){
                callback(placesObjects);
            }else{
                callback(false);
            }
        }
    });
};

exports.returnMyEvents = function(callback){

    var Event = Parse.Object.extend("Event");

    // Create a query for places
    var query = new Parse.Query(Event);
    query.ascending("date");

    var today = new Date();
    today.setHours(00,00);
    query.greaterThanOrEqualTo("date",today);

    query.equalTo("createdby", api.user.objectId);
    // Final list of objects

    exports.apiCallback = function(){
        //ui.alert("Connection failure\nTry again soon.");
        var toast = Ti.UI.createNotification({
            message:"Connection failure\n Please, check your internet.",
            duration: Ti.UI.NOTIFICATION_DURATION_LONG
        });
        toast.show();
    };

    query.find({
        success: function(placesObjects) {
            if(placesObjects.length>0){
                callback(placesObjects);
            }else{
                callback(false);
            }
        }
    });
};

var cb = null;

exports.facebookRegister = function(callback){
    fb.logout();
    fb.authorize();
    fb.addEventListener('login', function(e) {
        callback();
    });
};

exports.onFBLogin = function(e,cb){
    var registerUser = {
        fullName:e.name,
        email:e.email,
        id:e.id,
        profileImg:"http://graph.facebook.com/" + e.id + "/picture?height=110&width=110"
    };

    cb(registerUser);
};


exports.subscribeDeviceToPush = function(){

    if (iosVersioniosVersion >= 8) {
        function registerForPush() {
            Ti.Network.registerForPushNotifications({
                success : successToken,
                error : console.log("error   " + this),
                callback : callbackPush
            });
            // Remove event listener once registered for push notifications
            Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
        };
    
        // Wait for user settings to be registered before registering for push notifications
        Ti.App.iOS.addEventListener('usernotificationsettings', registerForPush);
    
        // Register notification types to use
        Ti.App.iOS.registerUserNotificationSettings({
            types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
        });
    }else{
        Titanium.Network.registerForPushNotifications({
            types: [
                Titanium.Network.NOTIFICATION_TYPE_BADGE,
                Titanium.Network.NOTIFICATION_TYPE_ALERT,
                Titanium.Network.NOTIFICATION_TYPE_SOUND
            ],
            success:function(e)
            {
                successToken(e);
            },
            error:function(e)
            {
                console.log(e);
            },
            callback:function(e)
            {
                callbackPush(e);
            }
        });
    }

    function successToken(e){
        var request = Titanium.Network.createHTTPClient({
            onload: function(e) {
                Titanium.App.Properties.setBool("pushEnabled",true);
            },
            onerror: function(e) {
                Ti.API.info("Push Notifications registration with Parse failed. Error: " + e.error);
            }
        });

        var params = {
        'deviceType': 'ios',
        'deviceToken': e.deviceToken,
        'channels': ['all','user_'+api.user.id]
        };

        // Register device token with Parse
        request.open('POST', 'https://api.parse.com/1/installations', true);
        request.setRequestHeader('X-Parse-Application-Id', 'cdPnH39rSCjl600O9ocyMl1GhulwlBgBj7h6tccP');
        request.setRequestHeader('X-Parse-REST-API-Key', 'jAWDpjJmYFWK3wc2jUHqN6SZsJbbPXpVXKa6pCBt');
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify(params));
    }
    function callbackPush(e){
        console.log(e);
    };
};

exports.unSubscribeDeviceToPush = function(){
     Titanium.Network.registerForPushNotifications({
        types: [
            Titanium.Network.NOTIFICATION_TYPE_BADGE,
            Titanium.Network.NOTIFICATION_TYPE_ALERT,
            Titanium.Network.NOTIFICATION_TYPE_SOUND
        ],
        success:function(e)
        {
             var request = Titanium.Network.createHTTPClient({
                onload: function(e) {
                    Titanium.App.Properties.setBool("pushEnabled",false);
                },
                onerror: function(e) {
                    Ti.API.info("Push Notifications registration with Parse failed. Error: " + e.error);
                }
              });
              
              var params = {
                'deviceType': 'ios',
                'deviceToken': e.deviceToken,
                'channels': ['']//,'user_'+user.id]
              };
            
              // Register device token with Parse
              request.open('POST', 'https://api.parse.com/1/installations', true);
              request.setRequestHeader('X-Parse-Application-Id', 'nrIW8ScIgO7OzEUJF8y155jHUTNKevmXiqLZ8uZY');
              request.setRequestHeader('X-Parse-REST-API-Key', '9UvvV0VKmltVqBK9bPtSC8mmhCh3b7GshsOBPw8i');
              request.setRequestHeader('Content-Type', 'application/json');
              request.send(JSON.stringify(params));
        }
    });
};

