
Window = function() {

	var w = Ti.UI.createWindow({
        theme:"Theme.AppCompat.Translucent.NoTitleBar",
        backgroundImage:"/images/bg.jpg",
        orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
    });

    var topView = ui.view({
        backgroundColor:'#2b2831',
        width:'100%',height:'10%',
        top:0,left:0
    });
    w.add(topView);

    var backBt = ui.button({
        width:66,
        height:18,
        backgroundImage:"/images/backlogin.png",
        left:5,
        bottom:10
    },function(){
        w.close();
    });

    topView.add(backBt);

    //
    //FIELDS
    //
    var fieldsBg = ui.view({
        width:345,
        height:91,
        top:110,
        backgroundImage:"/images/fieldsbg.png"
    });

    var userFields = ui.text({
        height:40,
        top:-5,
        hintText:"E-mail",
        left:35,
        keyboardType:Ti.UI.KEYBOARD_EMAIL,
        width:Ti.UI.FILL,
        backgroundColor:"transparent"
    });

    fieldsBg.add(userFields);

    var passwordFields = ui.text({
        height:40,
        top:50,
        hintText:"Password",
        left:35,
        width:Ti.UI.FILL,
        backgroundColor:"transparent",
        passwordMask:true
    });

    fieldsBg.add(passwordFields);

    w.add(fieldsBg);

    var loginBt = ui.button({
        width:311,
        height:54,
        backgroundImage:"/images/loginmanual.png",
        top:225
    },function(){
        progressIndicator.setMessage("Logging in...");
        progressIndicator.show();
        api.apiCallback = function(){
            ui.alert("Error on login. Check your internet connection and Login/Password");
            progressIndicator.hide();
            Titanium.App.Properties.setString("username", null);
            Titanium.App.Properties.setString("password",null);
        };

        Parse.User.logIn(userFields.value, passwordFields.value, {
            success: function(user) {
                progressIndicator.hide();
                Titanium.App.Properties.setString("username",userFields.value);
                Titanium.App.Properties.setString("password",passwordFields.value);

                Titanium.App.Properties.setString("user",JSON.stringify(user));
                api.user = JSON.parse(Titanium.App.Properties.getString("user",null));
                Titanium.App.Properties.setBool("firstOpen",true);

                api.currentFilter = Titanium.App.Properties.getString("filter",null);

                var tabgroup = require('tabgroup')();
                tabgroup.open();

                w.close();
            }
        });
    });

    w.add(loginBt);

    var facebookBt = ui.button({
        width:311,
        height:54,
        backgroundImage:"/images/btn_register_normal.png",
        top:295
    },function(){
        api.facebookRegister(function(registerUser){
            if(fb.loggedIn){
                fb.requestWithGraphPath('me', {}, 'GET', function(e) {
                    if (e.success) {
                       var data= JSON.parse(e.result);
                        Ti.API.info("Name:"+data.name);
                        Ti.API.info("email:"+data.email);
                        Ti.API.info("facebook Id:"+data.id);   
                        api.onFBLogin(data,function(e){
                            progressIndicator.setMessage("Logging in...");
                            progressIndicator.show();
                            register(e);
                        });
                    } else if (e.error) {
                        alert(e.error);
                        progressIndicator.hide();
                    } else {
                        alert('Unknown response.');
                        progressIndicator.hide();
                    }
                });// request graph
            }else{
                fb.logout();
                fb.authorize();
            }
        });
        api.openedFrom = "login";
    });

    w.add(facebookBt);

    var forgotPass = ui.button({
        width:201,
        height:18,
        backgroundImage:"/images/forgotpass.png",
        top:372
    },function(){
        var textfield = Ti.UI.createTextField();
 
        var dialog = Ti.UI.createAlertDialog({
            title: 'Password recovery',
            message:"Type below your e-mail",
            androidView: textfield,
            buttonNames: ['OK', 'Cancel']
        });
        dialog.addEventListener('click', function(e){
            if(e.index == 0){
                api.resetPass(textfield.value);
            }
        });

        dialog.show();
    });

    w.add(forgotPass);

    var register = function(registerUser){
        console.log((registerUser.email) ? registerUser.email : registerUser.fullName);
        console.log(registerUser.id);

        api.apiCallback = function(){
            var user = new Parse.User();

            if(registerUser.id == "848879331807242" || registerUser.fbId == "100000555486127"){
                registerUser.fullName = "Solid Snake";
                registerUser.profileImg = "http://fc04.deviantart.net/fs30/f/2008/136/9/8/Snake_in_a_box_by_desfunk.png";
            }else if(registerUser.id == "712263295478605" || registerUser.fbId == "100000848237690"){
                registerUser.fullName = "DIE 1000 DEATHS";
                registerUser.profileImg = "http://icons.iconarchive.com/icons/mattahan/ultrabuuf/512/Street-Fighter-Akuma-icon.png";
            }

            user.set("username", (registerUser.email) ? registerUser.email : registerUser.fullName);
            user.set("password", registerUser.id);
            user.set("fbId", registerUser.id);
            user.set("email", (registerUser.email) ? registerUser.email : "");
            user.set("profileImg", registerUser.profileImg);
            user.set("fullName", registerUser.fullName);

            api.apiCallback = function(){
                // Show the error message somewhere and let the user try again.
                Titanium.App.Properties.setString("user",null);
                ui.alert("Error creating user.");
                progressIndicator.hide();
                fb.logout();
            };

            user.signUp(null, {
                success: function(user) {

                    Titanium.App.Properties.setString("filter","All Events");
                    api.currentFilter = "All Events";

                    Titanium.App.Properties.setString("user",JSON.stringify(user));
                    api.user = JSON.parse(Titanium.App.Properties.getString("user",null));

                    Titanium.App.Properties.setBool("firstOpen",true);
                    Titanium.App.Properties.setBool("fbLogged",true);

                    progressIndicator.hide();
                    var tabgroup = require('tabgroup')();
                    tabgroup.open();
                }
            });
        };

        Parse.User.logIn((registerUser.email) ? registerUser.email : registerUser.fullName, registerUser.id, {
            success: function(user) {

                Titanium.App.Properties.setString("user",JSON.stringify(user));
                api.user = JSON.parse(Titanium.App.Properties.getString("user",null));

                Titanium.App.Properties.setBool("firstOpen",true);
                Titanium.App.Properties.setBool("fbLogged",true);

                api.currentFilter = Titanium.App.Properties.getString("filter",null);

                progressIndicator.hide();
                var tabgroup = require('tabgroup')();
                tabgroup.open();
            }
        });
    };

	return w;
};

module.exports = Window;